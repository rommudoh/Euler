      **
      ** Copyright (c) 2016 Julian Leyh <julian@vgai.de>
      **
      ** Permission to use, copy, modify, and distribute this software for any
      ** purpose with or without fee is hereby granted, provided that the above
      ** copyright notice and this permission notice appear in all copies.
      **
      ** THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
      ** WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
      ** MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
      ** ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
      ** WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
      ** ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
      ** OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
      **

       IDENTIFICATION DIVISION.
       AUTHOR. Julian Leyh <julian@vgai.de>.
       PROGRAM-ID. euler-002.

       ENVIRONMENT DIVISION.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 Total PIC 9(9) VALUE 0.
       01 Next-Fib PIC 9(9) VALUE 0.
       01 Temp PIC 9(9).
       01 Rem PIC 9(9).

       PROCEDURE DIVISION.
           DISPLAY "Euler 002".

           PERFORM UNTIL Next-Fib > 4000000
               CALL 'fibonacci' USING Next-Fib
               DIVIDE Next-Fib BY 2 GIVING Temp REMAINDER Rem
               IF Rem = 0 THEN
                   ADD Next-Fib TO Total
               END-IF
           END-PERFORM.

           DISPLAY "Sum: " Total.

       STOP RUN.
