      **
      ** Copyright (c) 2016 Julian Leyh <julian@vgai.de>
      **
      ** Permission to use, copy, modify, and distribute this software for any
      ** purpose with or without fee is hereby granted, provided that the above
      ** copyright notice and this permission notice appear in all copies.
      **
      ** THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
      ** WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
      ** MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
      ** ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
      ** WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
      ** ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
      ** OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
      **

       IDENTIFICATION DIVISION.
       AUTHOR. Julian Leyh <julian@vgai.de>.
       PROGRAM-ID. euler-001.
       
       ENVIRONMENT DIVISION.
       
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 Total PIC 9(6) VALUE IS 0.
       01 Current PIC 9(6) VALUE IS 0.
       01 Maximum PIC 9(6) VALUE IS 0.
       01 Rem-3 PIC 9.
       01 Rem-5 PIC 9.
       01 Temp PIC 9(6).
       
       PROCEDURE DIVISION.
       DISPLAY "Euler 001".
       DISPLAY "Enter Maximum:"
       ACCEPT Maximum.
       PERFORM VARYING Current FROM 1 BY 1
           UNTIL Current >= Maximum
           DIVIDE Current BY 3 GIVING Temp REMAINDER Rem-3
           DIVIDE Current BY 5 GIVING Temp REMAINDER Rem-5
           IF (Rem-3 = 0) OR (Rem-5 = 0) THEN
               ADD Current TO Total
           END-IF
       END-PERFORM.
       DISPLAY "Sum: " Total.
       STOP RUN.
