      **
      ** Copyright (c) 2016 Julian Leyh <julian@vgai.de>
      **
      ** Permission to use, copy, modify, and distribute this software for any
      ** purpose with or without fee is hereby granted, provided that the above
      ** copyright notice and this permission notice appear in all copies.
      **
      ** THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
      ** WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
      ** MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
      ** ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
      ** WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
      ** ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
      ** OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
      **

       IDENTIFICATION DIVISION.
       AUTHOR. Julian Leyh <julian@vgai.de>.
       PROGRAM-ID. euler-003.

       ENVIRONMENT DIVISION.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 Input-Number PIC 9(12) VALUE 600851475143.
       01 Current-Value PIC 9(12).
       01 Maximum-Factor PIC 9(6) VALUE 1.
       01 Current-Factor PIC 9(6) VALUE 3.
       01 Rem PIC 9(6).
       01 Temp PIC 9(12).

       PROCEDURE DIVISION.
           DISPLAY "Euler 003".
      *    DISPLAY "Input Number:".
      *    ACCEPT Input-Number.

           MOVE Input-Number TO Current-Value.
           PERFORM UNTIL Current-Factor ** 2 > Input-Number
               MOVE 0 TO Rem
               PERFORM UNTIL Rem IS NOT EQUAL 0
                   DIVIDE Current-Value BY Current-Factor GIVING Temp
                       REMAINDER Rem
                   IF REM = 0 THEN
                       MOVE Current-Factor TO Maximum-Factor
                       MOVE Temp TO Current-Value
                   END-IF
               END-PERFORM
               ADD 2 TO Current-Factor
           END-PERFORM.

           DISPLAY "Maximum Factor: " Maximum-Factor.

       STOP RUN.
