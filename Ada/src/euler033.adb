--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler033 is
   type Fraction is record
      Nominator : Natural := 0;
      Denominator : Natural := 0;
   end record;
   procedure Normalize (Number : in out Fraction) is
      X, Y : Natural;
      GGT : Natural;
   begin
      if Number.Nominator > Number.Denominator then
         X := Number.Nominator;
         Y := Number.Denominator;
      else
         X := Number.Denominator;
         Y := Number.Nominator;
      end if;
      GGT := Y;
      while X mod Y /= 0 loop
         GGT := X mod Y;
         X := Y;
         Y := GGT;
      end loop;
      Number.Nominator := Number.Nominator / GGT;
      Number.Denominator := Number.Denominator / GGT;
   end Normalize;
   function "*" (Left, Right : Fraction) return Fraction is
      Result : constant Fraction := (Nominator => Left.Nominator * Right.Nominator, Denominator => Left.Denominator * Right.Denominator);
   begin
      return Result;
   end "*";
   overriding
   function "=" (Left, Right : Fraction) return Boolean is
   begin
      if Left.Denominator = 0 or else Right.Denominator = 0 then
         return False;
      end if;
      if Left.Nominator = 0 or else Right.Nominator = 0 then
         return False;
      end if;
      return Left.Nominator * Right.Denominator = Left.Denominator * Right.Nominator;
   end "=";
   X, C : Natural := 0;
   Result : Fraction := (Nominator => 1, Denominator => 1);
begin
   for A in 10 .. 100 loop
      for B in 10 .. A - 1 loop
         -- Bruch: B / A
         -- prüfen, ob A und B eine gleiche Ziffer erhalten:
         -- Ziffer = A mod 10 => C := A / 10
         -- Ziffer = A / 10 => C := A mod 10
         -- Ziffer = B mod 10 => D := B / 10
         -- Ziffer = B / 10 => D := B mod 10
         X := A mod 10;
         C := A / 10;
         if X /= 0 then
            if B mod 10 = X then
               if Fraction'(Nominator => B, Denominator => A) = Fraction'(Nominator => B / 10, Denominator => C) then
                  Result := Result * Fraction'(Nominator => B, Denominator => A);
                  Ada.Text_IO.Put_Line ("Bruch:" & Integer'Image (B) & " /" & Integer'Image (A));
               end if;
            elsif B / 10 = X then
               if Fraction'(Nominator => B, Denominator => A) = Fraction'(Nominator => B mod 10, Denominator => C) then
                  Result := Result * Fraction'(Nominator => B, Denominator => A);
                  Ada.Text_IO.Put_Line ("Bruch:" & Integer'Image (B) & " /" & Integer'Image (A));
               end if;
            end if;
         end if;

         X := A / 10;
         C := A mod 10;
            if B mod 10 = X then
               if Fraction'(Nominator => B, Denominator => A) = Fraction'(Nominator => B / 10, Denominator => C) then
                  Result := Result * Fraction'(Nominator => B, Denominator => A);
                  Ada.Text_IO.Put_Line ("Bruch:" & Integer'Image (B) & " /" & Integer'Image (A));
               end if;
            elsif B / 10 = X then
               if Fraction'(Nominator => B, Denominator => A) = Fraction'(Nominator => B mod 10, Denominator => C) then
                  Result := Result * Fraction'(Nominator => B, Denominator => A);
                  Ada.Text_IO.Put_Line ("Bruch:" & Integer'Image (B) & " /" & Integer'Image (A));
               end if;
            end if;
      end loop;
   end loop;
   Normalize (Result);
   Ada.Text_IO.Put_Line ("Euler 33:" & Integer'Image (Result.Nominator) & " /" & Integer'Image (Result.Denominator));
end Euler033;
