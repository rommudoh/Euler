--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Ada.Text_IO;
procedure Euler031 is
   type Coins is (One_P, Two_P, Five_P, Ten_P, Twenty_P, Fifty_P, One_Pound, Two_Pound);
   type Coin_Set is array (Coins) of Natural;
   package Coin_Set_Vectors is new Ada.Containers.Vectors
     (Element_Type => Coin_Set, Index_Type => Positive);
   Values : constant Coin_Set := (One_P     => 1,
                                  Two_P     => 2,
                                  Five_P    => 5,
                                  Ten_P     => 10,
                                  Twenty_P  => 20,
                                  Fifty_P   => 50,
                                  One_Pound => 100,
                                  Two_Pound => 200);
   function Count_Combinations (Amount : Natural) return Natural is
      Current_Value : Coin_Set;
      Value         : Natural := Amount;
      Combinations  : Coin_Set_Vectors.Vector;
   begin
      for Two_Pounds in 0 .. Value / Values (Two_Pound) loop
         Value := Amount - Two_Pounds * Values (Two_Pound);
         for One_Pounds in 0 .. Value / Values (One_Pound) loop
            Value := Value - One_Pounds * Values (One_Pound);
            for Fifty_Ps in 0 .. Value / Values (Fifty_P) loop
               Value := Value - Fifty_Ps * Values (Fifty_P);
               for Twenty_Ps in 0 .. Value / Values (Twenty_P) loop
                  Value := Value - Twenty_Ps * Values (Twenty_P);
                  for Ten_Ps in 0 .. Value / Values (Ten_P) loop
                     Value := Value - Ten_Ps * Values (Ten_P);
                     for Five_Ps in 0 .. Value / Values (Five_P) loop
                        Value := Value - Five_Ps * Values (Five_P);
                        for Two_Ps in 0 .. Value / Values (Two_P) loop
                           Value := Value - Two_Ps * Values (Two_P);
                           Current_Value := (Value, Two_Ps, Five_Ps, Ten_Ps, Twenty_Ps, Fifty_Ps, One_Pounds, Two_Pounds);
                           if not Combinations.Contains (Current_Value) then
                              Combinations.Append (Current_Value);
                           end if;
                           Value := Value + Two_Ps * Values (Two_P);
                        end loop;
                        Value := Value + Five_Ps * Values (Five_P);
                     end loop;
                     Value := Value + Ten_Ps * Values (Ten_P);
                  end loop;
                  Value := Value + Twenty_Ps * Values (Twenty_P);
               end loop;
               Value := Value + Fifty_Ps * Values (Fifty_P);
            end loop;
            Value := Value + One_Pounds * Values (One_Pound);
         end loop;
      end loop;
      return Natural (Combinations.Length);
   end Count_Combinations;
begin
   Ada.Text_IO.Put_Line ("Euler 31:" & Integer'Image (Count_Combinations (200)));
end Euler031;
