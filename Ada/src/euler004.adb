--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Strings.Fixed;
with Ada.Text_IO;
procedure Euler004 is
   function Is_Palindrome (Number : Natural) return Boolean is
      Image : constant String := Ada.Strings.Fixed.Trim (Natural'Image (Number), Ada.Strings.Both);
      Left  : Positive := Image'First;
      Right : Positive := Image'Last;
   begin
      while Left < Right loop
         if Image (Left) /= Image (Right) then
            return False;
         end if;
         Left := Left + 1;
         Right := Right - 1;
      end loop;
      return True;
   end Is_Palindrome;
   First, Second : Positive := 1;
   Max_Palindrome : Positive := 1;
begin
   for M in 100 .. 999 loop
      for N in 100 .. M loop
         if Is_Palindrome (M * N) and then M * N > Max_Palindrome then
            Max_Palindrome := M * N;
            First := M;
            Second := N;
         end if;
      end loop;
   end loop;
   Ada.Text_IO.Put_Line ("Maximum Palindrome:" & Integer'Image (Max_Palindrome));
   Ada.Text_IO.Put_Line ("(" & Integer'Image (First) & "," & Integer'Image (Second) & ")");
end Euler004;
