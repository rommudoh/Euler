--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

package Fractions is
   type Fraction is private;

   -- create fraction
   function Create (Nominator : Integer; Denominator : Integer := 1) return Fraction;
   function "/" (Left, Right : Integer) return Fraction;

   -- get values
   function Nominator (X : Fraction) return Integer;
   function Denominator (X : Fraction) return Positive;
   function Sign (X : Fraction) return Integer;
   function Sign (X : Fraction) return Float;

   -- conversions
   function Image (X : Fraction) return String;
   function To_Float (X : Fraction) return Float;

   -- inversions
   function "-" (X : Fraction) return Fraction;
   function Inverse (X : Fraction) return Fraction;

   -- arithmetic with fractions
   function "+" (Left, Right : Fraction) return Fraction;
   function "-" (Left, Right : Fraction) return Fraction;
   function "*" (Left, Right : Fraction) return Fraction;
   function "/" (Left, Right : Fraction) return Fraction;

   -- arithmetic with fraction and integer
   function "+" (Left : Fraction; Right : Integer) return Fraction;
   function "-" (Left : Fraction; Right : Integer) return Fraction;
   function "*" (Left : Fraction; Right : Integer) return Fraction;
   function "/" (Left : Fraction; Right : Integer) return Fraction;
   function "+" (Left : Integer; Right : Fraction) return Fraction;
   function "-" (Left : Integer; Right : Fraction) return Fraction;
   function "*" (Left : Integer; Right : Fraction) return Fraction;
   function "/" (Left : Integer; Right : Fraction) return Fraction;

   -- compare fractions
   overriding
   function "=" (Left, Right : Fraction) return Boolean;
   function ">" (Left, Right : Fraction) return Boolean;
   function "<" (Left, Right : Fraction) return Boolean;
   function ">=" (Left, Right : Fraction) return Boolean;
   function "<=" (Left, Right : Fraction) return Boolean;

   -- compare fraction with integer
   function "=" (Left : Fraction; Right : Integer) return Boolean;
   function ">" (Left : Fraction; Right : Integer) return Boolean;
   function "<" (Left : Fraction; Right : Integer) return Boolean;
   function ">=" (Left : Fraction; Right : Integer) return Boolean;
   function "<=" (Left : Fraction; Right : Integer) return Boolean;

   -- compare fraction with float
   function "=" (Left : Fraction; Right : Float) return Boolean;
   function ">" (Left : Fraction; Right : Float) return Boolean;
   function "<" (Left : Fraction; Right : Float) return Boolean;
   function ">=" (Left : Fraction; Right : Float) return Boolean;
   function "<=" (Left : Fraction; Right : Float) return Boolean;

   -- exceptions
   Division_By_Zero : exception;
private
--     type Number is mod 2**64;
--     function Sign (X : Fraction) return Number;
   type Fraction is record
      Nominator : Natural;
      Denominator : Positive;
      Negative : Boolean;
   end record;
end Fractions;
