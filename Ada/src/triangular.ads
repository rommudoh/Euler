--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Finalization;
package Triangular is
   type Pyramid is new Ada.Finalization.Limited_Controlled with private;
   procedure Create_Pyramid (Item : in out Pyramid; Height : Positive);
   overriding procedure Finalize (Object : in out Pyramid);
   overriding procedure Initialize (Object : in out Pyramid);
   function Row_Count (Item : Pyramid) return Positive;
   function Column_Count (Item : Pyramid; Row : Positive) return Positive;
   procedure Set (Item : in out Pyramid; Row, Column : Positive; Value : Natural);
   function Get (Item : Pyramid; Row, Column : Positive) return Natural;
private
   type Row is array (Positive range <>) of Natural;
   type Row_Access is access Row;
   type Row_Access_Array is array (Positive range <>) of Row_Access;
   type Row_Access_Array_Access is access Row_Access_Array;
   type Pyramid is new Ada.Finalization.Limited_Controlled with record
      Rows : Row_Access_Array_Access;
   end record;
end Triangular;
