--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler021 is
   function Sum_Of_Divisors (N : Natural) return Natural is
      Sum   : Natural := 0;
   begin
      for I in 1 .. N / 2 loop
         if N mod I = 0 then
            Sum := Sum + I;
         end if;
      end loop;
      return Sum;
   end Sum_Of_Divisors;
   Divisor_Sum : array (1 .. 10_000) of Natural;
   Sum_Of_Numbers : Natural := 0;
begin
   for I in Divisor_Sum'Range loop
      Divisor_Sum (I) := Sum_Of_Divisors (I);
   end loop;
   for I in Divisor_Sum'Range loop
      if Divisor_Sum (I) in Divisor_Sum'Range then
         if Divisor_Sum (Divisor_Sum (I)) = I and then Divisor_Sum (I) /= I then
            Ada.Text_IO.Put_Line ("Pair:" & Integer'Image (I) & "," & Integer'Image (Divisor_Sum (I)));
            Sum_Of_Numbers := Sum_Of_Numbers + I;
         end if;
      else
         if I = Sum_Of_Divisors (Divisor_Sum (I)) then
            Ada.Text_IO.Put_Line ("Pair out of bounds:" & Integer'Image (I) & "," & Integer'Image (Sum_Of_Divisors (Divisor_Sum (I))));
         end if;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Sum:" & Integer'Image (Sum_Of_Numbers));
end Euler021;
