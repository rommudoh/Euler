--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

package body Fibonacci is
   Maximum_Fibonaccis : constant := 10000;
   Fibonacci_Array    : array (1 .. Maximum_Fibonaccis) of Numbers.Number := (1      => Numbers.To_Big_Number (1),
                                                                              2      => Numbers.To_Big_Number (1),
                                                                              others => Numbers.To_Big_Number (0));
   Last_Fibonacci     : Natural := 2;
   function Get_Next_Fibonacci return Numbers.Number is
      use type Numbers.Number;
      Result : Numbers.Number;
   begin
      Result := Fibonacci_Array (Last_Fibonacci) + Fibonacci_Array (Last_Fibonacci - 1);
      Last_Fibonacci := Last_Fibonacci + 1;
      Fibonacci_Array (Last_Fibonacci) := Result;
      return Result;
   end Get_Next_Fibonacci;
   function Get_Nth_Fib (N : Positive) return Numbers.Number is
      Temporary : Numbers.Number;
      pragma Unreferenced (Temporary);
   begin
      for X in Last_Fibonacci + 1 .. N loop
         Temporary := Get_Next_Fibonacci;
      end loop;
      return Fibonacci_Array (N);
   end Get_Nth_Fib;
end Fibonacci;
