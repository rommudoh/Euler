--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Primes;
procedure Euler041 is
   package Primes_10_Digit is new Primes (Natural, 1_000_000_000);
   function Next_Permutation (Number : Natural) return Natural is
      type Digit_Array is array (Positive range <>) of Natural;
      Num_Digits : Natural := 0;
      Value      : Natural := Number;
   begin
      while Value > 0 loop
         Num_Digits := Num_Digits + 1;
         Value := Value / 10;
      end loop;
      declare
         Position_Digits : Digit_Array (1 .. Num_Digits);
         Result          : Natural := 0;
      begin
         for I in Position_Digits'Range loop
            Position_Digits (I) := (Number / 10 ** (I - 1)) mod 10;
         end loop;
         declare
            K : Natural := Position_Digits'Last - 1;
            L : Positive := Position_Digits'Last;
         begin
            -- find largest K for element K < element K+1
            loop
               if K = 0 then
                  return Number; -- or raise No_More_Permutations
               end if;
               exit when Position_Digits (K) < Position_Digits (K + 1);
               K := K - 1;
            end loop;
            -- find largest L for element K < element L
            loop
               exit when Position_Digits (K) < Position_Digits (L);
               L := L - 1;
            end loop;
            -- swap elements K and L
            declare
               Temporary : constant Natural := Position_Digits (K);
            begin
               Position_Digits (K) := Position_Digits (L);
               Position_Digits (L) := Temporary;
            end;
            -- reverse elements K+1 .. Last
            declare
               Temporary : constant Digit_Array (K + 1 .. Position_Digits'Last) := Position_Digits (K + 1 .. Position_Digits'Last);
            begin
               for I in Temporary'Range loop
                  Position_Digits (Position_Digits'Last - I + K + 1) := Temporary (I);
               end loop;
            end;
         end;
         for I in Position_Digits'Range loop
            Result := Result + 10 ** (I - 1) * Position_Digits (I);
         end loop;
         return Result;
      end;
   end Next_Permutation;
   Max_Prime : Natural := 0;
   Init_Value : constant Natural := 987654321;
begin
   Ada.Text_IO.Put_Line ("Euler041:");
   for Exponent in 1 .. 8 loop
      declare
         Current_Prime_Test : Positive := Init_Value mod 10 ** Exponent;
         Previous_Prime_Test : Natural := 0;
      begin
         loop
            if Current_Prime_Test > Max_Prime and then Primes_10_Digit.Is_Prime (Current_Prime_Test) then
               Ada.Text_IO.Put_Line (Integer'Image (Current_Prime_Test));
               Max_Prime := Current_Prime_Test;
            end if;
            Current_Prime_Test := Next_Permutation (Current_Prime_Test);
            exit when Current_Prime_Test = Previous_Prime_Test;
            Previous_Prime_Test := Current_Prime_Test;
         end loop;
      end;
   end loop;
   Ada.Text_IO.Put_Line (Integer'Image (Max_Prime));
end Euler041;
