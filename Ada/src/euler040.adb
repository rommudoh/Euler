--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler040 is
   function Count_Digits (N : Natural) return Natural is
      Result : Natural := 0;
      Value  : Natural := N;
   begin
      while Value > 0 loop
         Result := Result + 1;
         Value := Value / 10;
      end loop;
      return Result;
   end Count_Digits;
   function Get_Nth_Digit (N : Natural; I : Natural) return Natural is
   begin
      return N / 10 ** (Count_Digits (N) - I) mod 10;
   end Get_Nth_Digit;

   Current_Position : Natural := 0;
   Current_Value    : Natural := 1;

   function Get_Digit (Index : Positive) return Natural is
      Current_Offset : Natural := Index - Current_Position;
   begin
      while Current_Offset > Count_Digits (Current_Value) loop
         Current_Position := Current_Position + Count_Digits (Current_Value);
         Current_Value := Current_Value + 1;
         Current_Offset := Index - Current_Position;
      end loop;
      return Get_Nth_Digit (Current_Value, Current_Offset);
   end Get_Digit;

   Digit_1 : constant Natural := Get_Digit (10 ** 0);
   Digit_2 : constant Natural := Get_Digit (10 ** 1);
   Digit_3 : constant Natural := Get_Digit (10 ** 2);
   Digit_4 : constant Natural := Get_Digit (10 ** 3);
   Digit_5 : constant Natural := Get_Digit (10 ** 4);
   Digit_6 : constant Natural := Get_Digit (10 ** 5);
   Digit_7 : constant Natural := Get_Digit (10 ** 6);
begin
   Ada.Text_IO.Put_Line ("Euler 40");
--     for I in 1 .. 10 loop
--        Ada.Text_IO.Put (Integer'Image (Get_Digit (I)));
--     end loop;
   Ada.Text_IO.Put_Line
     (Integer'Image (Digit_1) & " *" &
      Integer'Image (Digit_2) & " *" &
      Integer'Image (Digit_3) & " *" &
      Integer'Image (Digit_4) & " *" &
      Integer'Image (Digit_5) & " *" &
      Integer'Image (Digit_6) & " *" &
      Integer'Image (Digit_7) & " =" &
      Integer'Image (Digit_1 * Digit_2 * Digit_3 * Digit_4 * Digit_5 * Digit_6 * Digit_7));
end Euler040;
