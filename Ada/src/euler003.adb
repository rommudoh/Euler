--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Ada.Text_IO;
with Ada.Numerics.Elementary_Functions;
procedure Euler003 is
   type My_Type is range 1 .. 600851475143;
   package Num_Vectors is new Ada.Containers.Vectors (Element_Type => My_Type, Index_Type => Positive);
   use type Num_Vectors.Cursor;
   function Get_Prime_Factors (Number : My_Type) return Num_Vectors.Vector is
      Result  : Num_Vectors.Vector;
      Divisor : My_Type := 3;
      Value   : My_Type := Number;
      Root : Float := Ada.Numerics.Elementary_Functions.Sqrt (Float (Number));
   begin
      while Divisor <= My_Type (Root) loop
         while Value mod Divisor = 0 loop
            Result.Append (Divisor);
            Value := Value / Divisor;
         end loop;
         Divisor := Divisor + 2;
      end loop;
      return Result;
   end Get_Prime_Factors;
   Prime_Factors : constant Num_Vectors.Vector := Get_Prime_Factors (600_851_475_143);
   Position : Num_Vectors.Cursor := Prime_Factors.First;
begin
   Ada.Text_IO.Put ("Prime factors:");
   while Position /= Num_Vectors.No_Element loop
      Ada.Text_IO.Put (My_Type'Image (Num_Vectors.Element (Position)) & ",");
      Num_Vectors.Next (Position);
   end loop;
   Ada.Text_IO.New_Line;
end Euler003;
