--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler007 is
   Maximum_Primes : constant := 10000;
   Primes : array (1 .. Maximum_Primes) of Natural := (1 => 2, others => 1);
   Last_Prime : Natural := 0;
   function Get_Next_Prime return Natural is
      function Is_Prime (N : Natural) return Boolean is
      begin
         for I in 1 .. Last_Prime loop
            exit when Primes (I) * Primes (I) > N;
            if N mod Primes (I) = 0 then
               return False;
            end if;
         end loop;
         return True;
      end Is_Prime;
      Result : Natural;
   begin
      -- first call
      if Last_Prime = 0 then
         Last_Prime := 1;
         return 2;
      end if;
      -- call N + 1
      Result := Primes (Last_Prime);
      -- make it uneven
      if Result = 2 then
         Result := 1;
      end if;
      -- continue search
      loop
         Result := Result + 2;
         exit when Is_Prime (Result);
      end loop;
      Last_Prime := Last_Prime + 1;
      Primes (Last_Prime) := Result;
      return Result;
   end Get_Next_Prime;
   Prime : Natural;
begin
   for I in 1 .. Maximum_Primes loop
      Prime := Get_Next_Prime;
   end loop;
   Ada.Text_IO.Put_Line ("Prime:" & Integer'Image (Prime));
end Euler007;
