--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Ada.Text_IO;
procedure Euler026 is
   package Natural_Vectors is new Ada.Containers.Vectors
     (Element_Type => Natural, Index_Type => Positive);
   function Count_Repeating_Length (Testant : Positive) return Natural is
      Remainders : Natural_Vectors.Vector;
      Current    : Natural := 1;
   begin
      Remainders.Append (Current);
      loop
         Current := Current * 10 mod Testant;
         exit when Current = 0;
         if Remainders.Find_Index (Current) > 0 then
            return Remainders.Last_Index - Remainders.Find_Index (Current) + 1;
         else
            Remainders.Append (Current);
         end if;
      end loop;
      return 0;
   end Count_Repeating_Length;
   Longest_Length : Natural := 0;
   The_Number : Natural := 0;
   Temporary : Natural;
begin
   for I in 1 .. 1000 loop
      Temporary := Count_Repeating_Length (I);
--      Ada.Text_IO.Put_Line ("I:" & Integer'Image (I) & " =" & Integer'Image (Temporary));
      if Temporary > Longest_Length then
         Longest_Length := Temporary;
         The_Number := I;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 26:" & Integer'Image (The_Number) & "^-1 has" & Integer'Image (Longest_Length) & " repeating digits.");
end Euler026;
