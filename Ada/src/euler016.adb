--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Big_Number;
procedure Euler016 is
   package Three_Hundered_Digits is new Big_Number (300);
   use Three_Hundered_Digits;
   My_Big_Number : Number := To_Big_Number (2);
begin
   for I in 2 .. 1000 loop
      My_Big_Number := My_Big_Number + My_Big_Number;
   end loop;
   declare
      Text : constant String := Image (My_Big_Number);
      Sum : Natural := 0;
   begin
      for I in Text'Range loop
         if Text (I) /= ' ' then
            Sum := Sum + Character'Pos (Text (I)) - Character'Pos ('0');
         end if;
      end loop;
      Ada.Text_IO.Put_Line ("Sum: " & Integer'Image (Sum));
   end;
   Ada.Text_IO.Put_Line ("My Big Number: " & Image (My_Big_Number));
end Euler016;
