--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Ada.Text_IO;
procedure Euler074 is
   package Natural_Vectors is new Ada.Containers.Vectors
     (Element_Type => Natural, Index_Type => Natural);
   Faculties : Natural_Vectors.Vector;
   function Faculty (N : Natural) return Natural is
   begin
      if Natural (Faculties.Length) = 0 then
         Faculties.Append (1);
         Faculties.Append (1);
      end if;
      if Natural (Faculties.Length) <= N then
         Faculties.Append (Faculty (N - 1) * N);
      end if;
      return Faculties.Element (N);
   end Faculty;
   function Digit_Faculty_Sum (N : Natural) return Natural is
      Value : Natural := N;
      Result : Natural := 0;
   begin
      while Value /= 0 loop
         Result := Result + Faculty (Value mod 10);
         Value := Value / 10;
      end loop;
      return Result;
   end Digit_Faculty_Sum;
   function Count_Non_Repeating_Terms (N : Natural) return Natural is
      Terms : Natural_Vectors.Vector;
      Next_Term : Natural := N;
   begin
      while not Terms.Contains (Next_Term) loop
         Terms.Append (Next_Term);
         Next_Term := Digit_Faculty_Sum (Next_Term);
      end loop;
      return Natural (Terms.Length);
   end Count_Non_Repeating_Terms;
   Count : Natural := 0;
begin
   Ada.Text_IO.Put_Line ("Euler 074:");
   for I in 1 .. 1_000_000 loop
      if Count_Non_Repeating_Terms (I) = 60 then
         Count := Count + 1;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Count:" & Integer'Image (Count));
end Euler074;
