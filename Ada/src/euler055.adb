--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Strings.Fixed;
with Ada.Text_IO;
with Big_Number;
procedure Euler055 is
   package My_Big is new Big_Number (100);
   use type My_Big.Number;
   function Is_Palindrome (Number : My_Big.Number) return Boolean is
      Image : constant String := Ada.Strings.Fixed.Trim (My_Big.Image (Number), Ada.Strings.Both);
      Left  : Positive := Image'First;
      Right : Positive := Image'Last;
   begin
      while Left < Right loop
         if Image (Left) /= Image (Right) then
            return False;
         end if;
         Left := Left + 1;
         Right := Right - 1;
      end loop;
      return True;
   end Is_Palindrome;
   function Reversed (Number : My_Big.Number) return My_Big.Number is
      Result : My_Big.Number;
      Image  : constant String := My_Big.Image (Number);
      Digit_Count : constant Natural := My_Big.Count_Digits (Number);

   begin
      for I in reverse Image'Last - Digit_Count + 1 .. Image'Last loop
         Result := Result * 10;
         Result := Result + My_Big.To_Big_Number (Natural'Value (Image (I .. I)));
      end loop;
      return Result;
   end Reversed;
   function Is_Lychrel (Number : Natural) return Boolean is
      Current : My_Big.Number := My_Big.To_Big_Number (Number);
   begin
      for Iteration in 1 .. 50 loop
         Current := Current + Reversed (Current);
         if Is_Palindrome (Current) then
            return False;
         end if;
      end loop;
      -- Ada.Text_IO.Put_Line ("Lychrel:" & Natural'Image (Number));
      return True;
   end Is_Lychrel;
   Count : Natural := 0;
begin
   Ada.Text_IO.Put_Line ("Euler055:");
   for A in 1 .. 10_000 loop
      if Is_Lychrel (A) then
         Count := Count + 1;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Count:" & Integer'Image (Count));
end Euler055;
