--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Numerics.Elementary_Functions;
with Ada.Text_IO;

procedure Euler064 is

   function Count_Period_Length (S : Integer) return Natural is
      m : Integer := 0;
      d : Integer := 1;
      a, a0 : Integer;
      period : Natural := 0;
   begin
      a0 := Integer (Ada.Numerics.Elementary_Functions.Sqrt (Float (S)));

      if a0 ** 2 = S then
         return 0;
      end if;

      a := a0;

      loop
         m := d * a - m;
         d := (S - m * m) / d;
         a := (a0 + m) / d;
         period := period + 1;
         exit when a = 2 * a0;
      end loop;

      return period;
   end Count_Period_Length;

   oddCount : Natural := 0;
begin
   for N in 2 .. 10_000 loop
      if Count_Period_Length (N) mod 2 = 1 then
         oddCount := oddCount + 1;
      end if;
   end loop;

   Ada.Text_IO.Put_Line ("Result: " & oddCount'Img);
end Euler064;
