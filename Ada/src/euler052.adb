--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Ada.Text_IO;
procedure Euler052 is
   package Natural_Vectors is new Ada.Containers.Vectors
     (Element_Type => Natural, Index_Type => Positive);
   package Natural_Sort is new Natural_Vectors.Generic_Sorting;
   function Get_Digits (N : Natural) return Natural_Vectors.Vector is
      Result : Natural_Vectors.Vector;
      Value : Natural := N;
   begin
      while Value > 0 loop
         Result.Append (Value mod 10);
         Value := Value / 10;
      end loop;
      Natural_Sort.Sort (Result);
      return Result;
   end Get_Digits;
   Current : Natural := 0;
begin
   loop
      Current := Current + 1;
      declare
         use type Natural_Vectors.Vector;
         Double : constant Natural_Vectors.Vector := Get_Digits (Current * 2);
         More   : Natural_Vectors.Vector := Get_Digits (Current * 3);
      begin
         if Double = More then
            More := Get_Digits (Current * 4);
            if Double = More then
               More := Get_Digits (Current * 5);
               if Double = More then
                  More := Get_Digits (Current * 6);
                  exit when Double = More;
               end if;
            end if;
         end if;
      end;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 52:" & Integer'Image (Current));
end Euler052;
