--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Strings.Fixed;
with Ada.Strings.Maps;
with Ada.Text_IO;
package body Big_Number is
   function To_Big_Number (Source : Natural) return Number is
      Result : Number;
      Value  : Natural := Source;
   begin
      while Value > 0 loop
         Result.Last_Index := Result.Last_Index + 1;
         -- oops, exceeded maximum digits
         if Result.Last_Index > Result.Millions'Last then
            raise Constraint_Error with "Value exceeds range!";
         end if;
         Result.Millions (Result.Last_Index) := Million_Number (Value mod 1_000_000);
         Value := Value / 1_000_000;
      end loop;
      return Result;
   end To_Big_Number;
   function Image (Item : Number) return String is
      use Ada.Strings;
      package Million_IO is new Ada.Text_IO.Modular_IO (Million_Number);
      Result : String (1 .. 6 * Item.Last_Index);
      First : Positive := 1;
   begin
      if Item.Last_Index = 0 then
         Result := (others => '0');
      else
         for I in reverse 1 .. Item.Last_Index loop
            Million_IO.Put (To   => Result (First .. First + 5),
                            Item => Item.Millions (I),
                            Base  => 10);
            if First /= 1 then
               Fixed.Translate (Source  => Result (First .. First + 5),
                                Mapping => Maps.To_Mapping (From => " ",
                                                            To   => "0"));
            end if;
            First := First + 6;
         end loop;
      end if;
      return Result;
   end Image;
   -- default comparison operators: Equality
   overriding function "=" (Left, Right : Number) return Boolean is
   begin
      -- if last index not same, value is not same either
      if Left.Last_Index /= Right.Last_Index then
         return False;
      end if;
      -- if any of the digits differ, they are not equal
      for I in 1 .. Left.Last_Index loop
         if Left.Millions (I) /= Right.Millions (I) then
            return False;
         end if;
      end loop;
      return True;
   end "=";
   -- default comparison operators: Greater Than
   function ">" (Left, Right : Number) return Boolean is
   begin
      -- left has more digits than right
      if Left.Last_Index > Right.Last_Index then
         return True;
      end if;
      -- right has more digits than left
      if Left.Last_Index < Right.Last_Index then
         return False;
      end if;
      -- same digits, compare value of digits
      for I in reverse 1 .. Left.Last_Index loop
         if Left.Millions (I) < Right.Millions (I) then
            return False;
         elsif Left.Millions (I) > Right.Millions (I) then
            return True;
         end if;
      end loop;
      return False;
   end ">";
   -- default comparison operators: Greater or Equal
   function ">=" (Left, Right : Number) return Boolean is
   begin
      return Left = Right or else Left > Right;
   end ">=";
   -- default comparison operators: Less Than
   function "<" (Left, Right : Number) return Boolean is
   begin
      -- left has more digits than right
      if Left.Last_Index > Right.Last_Index then
         return False;
      end if;
      -- right has more digits than left
      if Left.Last_Index < Right.Last_Index then
         return True;
      end if;
      -- same digits, compare value of digits
      for I in reverse 1 .. Left.Last_Index loop
         if Left.Millions (I) < Right.Millions (I) then
            return True;
         elsif Left.Millions (I) > Right.Millions (I) then
            return False;
         end if;
      end loop;
      return False;
   end "<";
   -- default comparison operators: Less or Equal
   function "<=" (Left, Right : Number) return Boolean is
   begin
      return Left = Right or else Left < Right;
   end "<=";
   -- default arithmetic operators: Addition
   function "+" (Left, Right : Number) return Number is
      Result : Number := Left;
      Added_Numbers : Natural;
      Carrier : Natural := 0;
   begin
      -- add all right digits
      for I in 1 .. Right.Last_Index loop
         -- left had less digits, increase and simply copy
         if I > Result.Last_Index then
            Result.Last_Index := I;
            Result.Millions (I) := Right.Millions (I);
            -- if there was an overflow, add carrier
            if Carrier /= 0 then
               Added_Numbers := Natural (Result.Millions (I)) + Carrier;
               Carrier := Added_Numbers / 1_000_000;
               Result.Millions (I) := Million_Number (Added_Numbers mod 1_000_000);
            end if;
         else
            -- add numbers and carrier, recalculate carrier
            Added_Numbers := Natural (Result.Millions (I)) +
              Natural (Right.Millions (I)) + Carrier;
            Carrier := Added_Numbers / 1_000_000;
            Result.Millions (I) := Million_Number (Added_Numbers mod 1_000_000);
         end if;
      end loop;
      -- there is a carrier left!
      if Carrier /= 0 then
         -- if Left had more digits, propagate carrier
         for I in Right.Last_Index + 1 .. Result.Last_Index loop
            Added_Numbers := Natural (Result.Millions (I)) + Carrier;
            Carrier := Added_Numbers / 1_000_000;
            Result.Millions (I) := Million_Number (Added_Numbers mod 1_000_000);
         end loop;
      end if;
      -- still carrier left - increase digit count
      if Carrier /= 0 then
         Result.Last_Index := Result.Last_Index + 1;
         -- oops, exceeded maximum digits
         if Result.Last_Index > Result.Millions'Last then
            raise Constraint_Error with "Value exceeds range!";
         end if;
         Result.Millions (Result.Last_Index) := Million_Number (Carrier);
      end if;
      return Result;
   end "+";
   -- default arithmetic operators: Subtraction
   function "-" (Left, Right : Number) return Number is
      Result : Number := Left;
      Temporary : Integer;
      Carrier : Natural := 0;
   begin
      if Left < Right then
         raise Constraint_Error with "Result would be negative!";
      end if;
      for I in 1 .. Right.Last_Index loop
         Temporary := Integer (Result.Millions (I)) - Integer (Right.Millions (I)) - Carrier;
         if Temporary < 0 then
            Temporary := 1_000_000 - Temporary;
            Carrier := 1;
         else
            Carrier := 0;
         end if;
         Result.Millions (I) := Million_Number (Temporary);
      end loop;
      if Result.Millions (Result.Last_Index) = 0 then
         Result.Last_Index := Result.Last_Index - 1;
      end if;
      return Result;
   end "-";
   -- default arithmetic operators: Multiplication
   function "*" (Left : Number; Right : Natural) return Number is
      Result : Number;
   begin
      for I in 1 .. Right loop
         Result := Result + Left;
      end loop;
      return Result;
   end "*";
   -- default arithmetic operators: Multiplication
   function "*" (Left, Right : Number) return Number is
      Times : Number := Right;
      Result : Number := To_Big_Number (0);
   begin
      while Times > To_Big_Number (0) loop
         Result := Result + Left;
         Times := Times - To_Big_Number (1);
      end loop;
      return Result;
   end "*";
   -- complex arithmethic operator: Potence
   function "**" (Left : Number; Right : Natural) return Number is
      Result : Number := To_Big_Number (1);
   begin
      for I in 1 .. Right loop
         Result := Result * Left;
      end loop;
      return Result;
   end "**";
   -- count the digits
   function Count_Digits (Item : Number) return Natural is
      Result : Natural := 0;
      I : Natural := 6;
   begin
      Result := (Item.Last_Index - 1) * 6;
      loop
         I := I - 1;
         exit when I < 0 or else Natural (Item.Millions (Item.Last_Index)) / (10 ** I) /= 0;
      end loop;
      Result := Result + I + 1;
      return Result;
   end Count_Digits;
end Big_Number;
