--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Ada.Text_IO;
with Ada.Numerics.Elementary_Functions;
procedure Euler072 is
   type Number is mod 2 ** 64;
   package Num_Vectors is new Ada.Containers.Vectors (Element_Type => Number, Index_Type => Positive);
   use type Num_Vectors.Cursor;
   function Get_Prime_Factors (N : Number) return Num_Vectors.Vector is
      Result  : Num_Vectors.Vector;
      Divisor : Number := 3;
      Value   : Number := N;
      Root    : constant Float := Ada.Numerics.Elementary_Functions.Sqrt (Float (N));
   begin
      if N mod 2 = 0 then
         Result.Append (2);
      end if;
      while Divisor <= Number (Root) loop
         while Value mod Divisor = 0 loop
            Result.Append (Divisor);
            Value := Value / Divisor;
         end loop;
         Divisor := Divisor + 2;
      end loop;
      return Result;
   end Get_Prime_Factors;

   function Greatest_Common_Divisor (A, B : Positive) return Positive is
      Dividend  : Positive := A;
      Divisor   : Positive := B;
      Remainder : Natural;
   begin
      if A < B then
         Dividend := B;
         Divisor := A;
      end if;
      loop
         Remainder := Dividend mod Divisor;
         exit when Remainder = 0;
         Dividend := Divisor;
         Divisor := Remainder;
      end loop;
      return Divisor;
   end Greatest_Common_Divisor;
   Count     : Number := 0;
begin
   for I in Number range 2 .. 50000 loop -- denominator
      if I mod 1000 = 0 then
         Ada.Text_IO.Put_Line ("I =" & Number'Image (I) & ", Count =" & Number'Image (Count));
      end if;
      declare
         Primes : constant Num_Vectors.Vector := Get_Prime_Factors (I);
         Max    : array (Primes.First_Index .. Primes.Last_Index) of Natural;
      begin
         for I in Max'Range loop
            Max (I) := Natural (Ada.Numerics.Elementary_Functions.Log (X    => Float (I),
                                                                       Base => Float (Primes.Element (I))));
         end loop;
      end;
--        for J in 1 .. I - 1 loop -- numerator
--           if Greatest_Common_Divisor (I, J) = 1 then
--              Count := Count + 1;
--           end if;
--        end loop;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 072:" & Number'Image (Count));
end Euler072;
--  with Ada.Containers.Vectors;
--  with Ada.Text_IO;
--  with Ada.Numerics.Elementary_Functions;
--  procedure Euler003 is
--     type My_Type is range 1 .. 600851475143;
--     package Num_Vectors is new Ada.Containers.Vectors (Element_Type => My_Type, Index_Type => Positive);
--     use type Num_Vectors.Cursor;
--     function Get_Prime_Factors (Number : My_Type) return Num_Vectors.Vector is
--        Result  : Num_Vectors.Vector;
--        Divisor : My_Type := 3;
--        Value   : My_Type := Number;
--        Root : Float := Ada.Numerics.Elementary_Functions.Sqrt (Float (Number));
--     begin
--        while Divisor <= My_Type (Root) loop
--           while Value mod Divisor = 0 loop
--              Result.Append (Divisor);
--              Value := Value / Divisor;
--           end loop;
--           Divisor := Divisor + 2;
--        end loop;
--        return Result;
--     end Get_Prime_Factors;
--     Prime_Factors : constant Num_Vectors.Vector := Get_Prime_Factors (600_851_475_143);
--     Position : Num_Vectors.Cursor := Prime_Factors.First;
--  begin
--     Ada.Text_IO.Put ("Prime factors:");
--     while Position /= Num_Vectors.No_Element loop
--        Ada.Text_IO.Put (My_Type'Image (Num_Vectors.Element (Position)) & ",");
--        Num_Vectors.Next (Position);
--     end loop;
--     Ada.Text_IO.New_Line;
--  end Euler003;
