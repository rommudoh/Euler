--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Strings.Fixed;
with Ada.Text_IO;
procedure Euler036 is
   package Natural_IO is new Ada.Text_IO.Integer_IO (Natural);
   function Is_Palindrome (Word : String) return Boolean is
      Left  : Positive := Word'First;
      Right : Positive := Word'Last;
   begin
      while Left < Right loop
         if Word (Left) /= Word (Right) then
            return False;
         end if;
         Left := Left + 1;
         Right := Right - 1;
      end loop;
      return True;
   end Is_Palindrome;
   function Is_Palindrome (Number : Natural) return Boolean is
      Image_10 : constant String := Ada.Strings.Fixed.Trim (Natural'Image (Number), Ada.Strings.Both);
      Image_Base_2  : String (1 .. 23) := (others => ' ');
   begin
      Natural_IO.Put (To   => Image_Base_2,
                      Item => Number,
                      Base => 2);
      declare
         Image_2 : constant String := Ada.Strings.Fixed.Trim (Image_Base_2, Ada.Strings.Both);
      begin
         return Is_Palindrome (Image_10) and then Is_Palindrome (Image_2 (3 .. Image_2'Last - 1));
      end;
   end Is_Palindrome;
   Sum : Natural := 0;
begin
   for M in 1 .. 1_000_000 loop
      if Is_Palindrome (M) then
         Sum := Sum + M;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Palindromes:" & Integer'Image (Sum));
end Euler036;
