--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Numerics.Discrete_Random;
with Ada.Unchecked_Conversion;
with Ada.Text_IO;
procedure Euler323 is
   type Unsigned_Integer_64 is mod 2 ** 64;
   for Unsigned_Integer_64'Size use 64;

   type Unsigned_Integer_32 is mod 2 ** 32;
   for Unsigned_Integer_32'Size use 32;
   package Int_Random is new Ada.Numerics.Discrete_Random (Unsigned_Integer_32);

   type Bitset is array (1 .. 32) of Boolean;
   pragma Pack (Bitset);
   for Bitset'Size use 32;

   function Int_To_Bits is new Ada.Unchecked_Conversion (Unsigned_Integer_32, Bitset);
   function Bits_To_Int is new Ada.Unchecked_Conversion (Bitset, Unsigned_Integer_32);

   Generator : Int_Random.Generator;
   N : Natural := 0;
   V : Unsigned_Integer_64 := 10**12;
begin
   Ada.Text_IO.Put_Line ("Euler 323:");

   Int_Random.Reset (Generator);

   for I in 1 .. 100_000_000 loop
      declare
         Previous_X    : Unsigned_Integer_32 := 0;
         Current_X     : Unsigned_Integer_32;
         Current_Index : Natural := 0;
      begin
         loop
            Current_Index := Current_Index + 1;
            Current_X := Bits_To_Int (Int_To_Bits (Previous_X) or Int_To_Bits (Int_Random.Random (Generator)));
            exit when Current_X = Unsigned_Integer_32'Last;
            Previous_X := Current_X;
         end loop;
         N := N + Current_Index;
         Ada.Text_IO.Put_Line ("I =" & Integer'Image (Current_Index));
      end;
   end loop;
   Ada.Text_IO.Put_Line ("N =" & Float'Image (Float (N) / 1.0E+11));
end Euler323;
