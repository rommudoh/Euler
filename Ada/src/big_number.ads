--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

generic
   Max_Digit_Count : Positive;
package Big_Number is
   type Number is private;
   function To_Big_Number (Source : Natural) return Number;
   function Image (Item : Number) return String;
   -- default comparison operators: Equality
   overriding function "=" (Left, Right : Number) return Boolean;
   -- default comparison operators: Greater Than
   function ">" (Left, Right : Number) return Boolean;
   -- default comparison operators: Greater or Equal
   function ">=" (Left, Right : Number) return Boolean;
   -- default comparison operators: Less Than
   function "<" (Left, Right : Number) return Boolean;
   -- default comparison operators: Less or Equal
   function "<=" (Left, Right : Number) return Boolean;
   -- default arithmetic operators: Addition
   function "+" (Left, Right : Number) return Number;
   -- default arithmetic operators: Subtraction
   function "-" (Left, Right : Number) return Number;
   -- default arithmetic operators: Multiplication
   function "*" (Left : Number; Right : Natural) return Number;
   -- default arithmetic operators: Multiplication
   function "*" (Left, Right : Number) return Number;
   -- complex arithmethic operator: Potence
   function "**" (Left : Number; Right : Natural) return Number;
   -- count the digits
   function Count_Digits (Item : Number) return Natural;
private
   type Million_Number is mod 1_000_000;
   type Million_Array is array (Positive range <>) of Million_Number;
   type Number is record
      Millions   : Million_Array (1 .. Max_Digit_Count) := (others => 0);
      Last_Index : Natural := 0;
   end record;
end Big_Number;
