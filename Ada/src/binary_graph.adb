--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Unchecked_Deallocation;
package body Binary_Graph is
   overriding procedure Initialize (Object : in out Graph) is
   begin
      for I in Object.All_Nodes'Range loop
         Object.All_Nodes (I) := new Node;
      end loop;
      Object.Last_Index := 0;
   end Initialize;
   overriding procedure Finalize (Object : in out Graph) is
      procedure Free is new Ada.Unchecked_Deallocation
        (Object => Node, Name => Node_Access);
   begin
      for I in Object.All_Nodes'Range loop
         if Object.All_Nodes (I) /= null then
            Free (Object.All_Nodes (I));
         end if;
      end loop;
   end Finalize;
   procedure Insert (To : in out Graph; Value : Natural) is
   begin
      To.Last_Index := To.Last_Index + 1;
      if To.Last_Index > To.All_Nodes'Last then
         raise Graph_Full;
      end if;
      To.All_Nodes (To.Last_Index).Value := Value;
   end Insert;
   procedure Insert (To : in out Graph; Value : Natural; ID : out Positive) is
   begin
      To.Insert (Value);
      ID := To.Last_Index;
   end Insert;
   procedure Insert (To : in out Graph; Values : Natural_Array) is
   begin
      for I in Values'Range loop
         To.Insert (Values (I));
      end loop;
   end Insert;
   procedure Connect (On : in out Graph; From, Left, Right : Positive) is
   begin
      On.All_Nodes (From).Left := On.All_Nodes (Left);
      On.All_Nodes (From).Right := On.All_Nodes (Right);
   end Connect;
   procedure Connect (On : in out Graph; Connections : Connection_Array) is
   begin
      for I in Connections'Range loop
         On.Connect (From => Connections (I).From,
                     Left => Connections (I).Left,
                     Right => Connections (I).Right);
      end loop;
   end Connect;
end Binary_Graph;
