--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler017 is
   function To_Words (N : Natural) return String is
   begin
      -- single digits
      if N < 20 then
         case N is
            when 1 => return "one";
            when 2 => return "two";
            when 3 => return "three";
            when 4 => return "four";
            when 5 => return "five";
            when 6 => return "six";
            when 7 => return "seven";
            when 8 => return "eight";
            when 9 => return "nine";
            when 10 => return "ten";
            when 11 => return "eleven";
            when 12 => return "twelve";
            when 13 => return "thirteen";
            when 14 => return "fourteen";
            when 15 => return "fifteen";
            when 16 => return "sixteen";
            when 17 => return "seventeen";
            when 18 => return "eighteen";
            when 19 => return "nineteen";
            when others => null;
         end case;
      end if;
      if N > 19 and then N < 100 then
         case N / 10 is
            when 2 => return "twenty" & To_Words (N mod 10);
            when 3 => return "thirty" & To_Words (N mod 10);
            when 4 => return "forty" & To_Words (N mod 10);
            when 5 => return "fifty" & To_Words (N mod 10);
            when 6 => return "sixty" & To_Words (N mod 10);
            when 7 => return "seventy" & To_Words (N mod 10);
            when 8 => return "eighty" & To_Words (N mod 10);
            when 9 => return "ninety" & To_Words (N mod 10);
            when others => null;
         end case;
      end if;
      if N > 99 and then N < 1000 then
         if N mod 100 /= 0 then
            return To_Words (N / 100 mod 10) & "hundredand" & To_Words (N mod 100);
         else
            return To_Words (N / 100 mod 10) & "hundred";
         end if;
      end if;
      if N = 1000 then
         return "onethousand";
      end if;
      return "";
   end To_Words;
   Lengths : Natural := 0;
begin
   for I in 1 .. 1000 loop
      Lengths := Lengths + To_Words (I)'Length;
   end loop;
   Ada.Text_IO.Put_Line (Integer'Image (Lengths));
   Ada.Text_IO.Put_Line (To_Words (342));
end Euler017;
