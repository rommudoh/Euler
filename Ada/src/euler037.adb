--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Primes;
procedure Euler037 is
   package Primes_41m is new Primes (Number_Type   => Natural,
                                     Maximum_Prime => 41_000_000);
   Prime_Count   : Natural := 0;
   Prime         : Positive := 2;
   Sum           : Natural := 0;
begin
   Ada.Text_IO.Put_Line ("Euler037:");
   while Prime_Count < 11 loop
      if Prime > 10 and then Primes_41m.Is_Truncable_Prime (Prime) then
         Ada.Text_IO.Put_Line (Integer'Image (Prime));
         Prime_Count := Prime_Count + 1;
         Sum := Sum + Prime;
      end if;
      Prime := Primes_41m.Get_Next_Prime;
   end loop;
   Ada.Text_IO.Put_Line ("Sum:" & Integer'Image (Sum));
end Euler037;
