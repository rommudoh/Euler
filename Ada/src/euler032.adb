--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Ada.Text_IO;
with Permutations;
procedure Euler032 is
   subtype Digit is Positive range 1 .. 9;
   package Natural_Vectors is new Ada.Containers.Vectors
     (Element_Type => Natural, Index_Type => Positive);
   use type Natural_Vectors.Vector;
   package Vector_Sort is new Natural_Vectors.Generic_Sorting;
   package Digit_Permutations is new Permutations
     (Element_Type => Digit);
   function Get_Pandigitals (From : Digit_Permutations.Permutation) return Natural_Vectors.Vector is
      Result : Natural_Vectors.Vector;
      Left, Right, Product : Natural := 0;
   begin
      for Last_Left in From'First .. From'Last - 2 loop
         Left := 0;
         for I in From'First .. Last_Left loop
            Left := Left * 10;
            Left := Left + From (I);
         end loop;
         for Last_Right in Last_Left .. From'Last - 1 loop
            Right := 0;
            for I in Last_Left + 1 .. Last_Right loop
               Right := Right * 10;
               Right := Right + From (I);
            end loop;
            Product := 0;
            for I in Last_Right + 1 .. From'Last loop
               Product := Product * 10;
               Product := Product + From (I);
            end loop;
            if Left * Right = Product then
               if not Result.Contains (Product) then
                  Result.Append (Product);
               end if;
            end if;
         end loop;
      end loop;
      return Result;
   end Get_Pandigitals;
   Current_Permutation : Digit_Permutations.Permutation := (1, 2, 3, 4, 5, 6, 7, 8, 9);
   Pandigits : Natural_Vectors.Vector;
begin
   Digit_Permutations.Reset (Current_Permutation);
   loop
      declare
         New_Pandigits : Natural_Vectors.Vector := Get_Pandigitals (Current_Permutation);
      begin
         Vector_Sort.Merge (Pandigits, New_Pandigits);
         Digit_Permutations.Next (Current_Permutation);
      exception
         when Digit_Permutations.Last_Permutation =>
            exit;
      end;
   end loop;
   declare
      use type Natural_Vectors.Cursor;
      Pandigit_Sum : Natural := 0;
      Position : Natural_Vectors.Cursor := Pandigits.First;
   begin
      while Position /= Natural_Vectors.No_Element loop
         Ada.Text_IO.Put_Line ("Pandigit Product:" & Integer'Image (Natural_Vectors.Element (Position)));
         if Position = Pandigits.First or else Natural_Vectors.Element (Position) /= Natural_Vectors.Element (Natural_Vectors.Previous (Position)) then
            Pandigit_Sum := Pandigit_Sum + Natural_Vectors.Element (Position);
         end if;
         Natural_Vectors.Next (Position);
      end loop;
      Ada.Text_IO.Put_Line ("Euler 32:" & Integer'Image (Pandigit_Sum));
   end;
end Euler032;
