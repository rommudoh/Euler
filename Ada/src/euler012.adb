--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler012 is
   package Primes is
      function Get_Nth_Prime (N : Positive) return Natural;
   end Primes;
   package body Primes is
      Maximum_Primes : constant := 10000;
      Prime_Array    : array (1 .. Maximum_Primes) of Natural := (1 => 2, others => 1);
      Last_Prime     : Natural := 0;
      function Get_Next_Prime return Natural is
         function Is_Prime (N : Natural) return Boolean is
         begin
            for I in 1 .. Last_Prime loop
               exit when Prime_Array (I) * Prime_Array (I) > N;
               if N mod Prime_Array (I) = 0 then
                  return False;
               end if;
            end loop;
            return True;
         end Is_Prime;
         Result : Natural;
      begin
         -- first call
         if Last_Prime = 0 then
            Last_Prime := 1;
            return 2;
         end if;
         -- call N + 1
         Result := Prime_Array (Last_Prime);
         -- make it uneven
         if Result = 2 then
            Result := 1;
         end if;
         -- continue search
         loop
            Result := Result + 2;
            exit when Is_Prime (Result);
         end loop;
         Last_Prime := Last_Prime + 1;
         Prime_Array (Last_Prime) := Result;
         return Result;
      end Get_Next_Prime;
      function Get_Nth_Prime (N : Positive) return Natural is
         Temporary : Natural;
         pragma Unreferenced (Temporary);
      begin
         for X in Last_Prime + 1 .. N loop
            Temporary := Get_Next_Prime;
         end loop;
         return Prime_Array (N);
      end Get_Nth_Prime;
   end Primes;

   function Get_Nth_Triangle_Number (N : Positive) return Natural is
   begin
      return N * (N + 1) / 2;
   end Get_Nth_Triangle_Number;

   function Get_Factor_Count (N : Natural) return Positive is
      Result      : Positive := 1;
      Prime_Index : Natural := 1;
      Prime_Times : Natural;
      The_Number  : Natural;
   begin
      while Primes.Get_Nth_Prime (Prime_Index) ** 2 <= N loop
         Prime_Times := 1;
         The_Number := N;
         while The_Number mod Primes.Get_Nth_Prime (Prime_Index) = 0 loop
            Prime_Times := Prime_Times + 1;
            The_Number := The_Number / Primes.Get_Nth_Prime (Prime_Index);
         end loop;
         Result := Result * Prime_Times;
         Prime_Index := Prime_Index + 1;
      end loop;
      return Result;
   end Get_Factor_Count;

   The_Number : Natural;
   The_Factors : Positive;
begin
   for I in 1 .. 13000 loop
      The_Number := Get_Nth_Triangle_Number (I);
      The_Factors := Get_Factor_Count (The_Number);
      exit when The_Factors >= 500;
   end loop;
   Ada.Text_IO.Put (Integer'Image (The_Number) & " has" & Integer'Image (The_Factors) & " factors.");
   Ada.Text_IO.New_Line;
end Euler012;
