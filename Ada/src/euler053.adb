--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler053 is
   type Number is range 0 .. 2 ** 63 - 1;
   function Binomial_Coefficient (N, K : Number) return Number is
   begin
      if K = 0 then
         return 1;
      end if;
      if K * 2 > N then
         return Binomial_Coefficient (N, N - K);
      end if;
      declare
         Result : Number := N - K + 1;
      begin
         for I in 2 .. K loop
            Result := Result * (N - K + I) / I;
         end loop;
         return Result;
      end;
   end Binomial_Coefficient;
   Count : Natural := 0;
begin
   Ada.Text_IO.Put_Line ("Euler053:");
   N_Loop : for N in Number range 1 .. 100 loop
      R_Loop : for R in Number range 0 .. N / 2 + 1 loop
         if Binomial_Coefficient (N, R) > 1_000_000 then
            Count := Count + Natural (N - 2 * R + 1);
            exit R_Loop;
         end if;
      end loop R_Loop;
   end loop N_Loop;
   Ada.Text_IO.Put_Line ("Count:" & Integer'Image (Count));
end Euler053;
