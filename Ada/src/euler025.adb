--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Big_Number;
with Fibonacci;
procedure Euler025 is
   package Numbers is new Big_Number (200);
   package My_Fibonacci is new Fibonacci (Numbers);
   Current : Numbers.Number;
   Index : Positive := 1;
begin
   loop
      Current := My_Fibonacci.Get_Nth_Fib (Index);
      exit when Numbers.Count_Digits (Current) >= 1000;
      Index := Index + 1;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 25: Fib" & Integer'Image (Index) & " = " & Numbers.Image (Current));
end Euler025;
