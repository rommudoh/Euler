--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler075 is
   function Count_Triangles (L : Natural) return Natural is
      A, C : Long_Long_Integer;
      Count : Natural := 0;
   begin
      for B in 1 .. L / 2 loop
         A := Long_Long_Integer (L - 2 * B) * Long_Long_Integer (L) / 2 / Long_Long_Integer (L - B);
         if A >= Long_Long_Integer (B) then
            C := Long_Long_Integer (L) - A - Long_Long_Integer (B);
            if C > 0 and then A * A + Long_Long_Integer (B) * Long_Long_Integer (B) = C * C then
               Count := Count + 1;
            end if;
         end if;
      end loop;
      return Count;
   end Count_Triangles;
   Count : Natural := 0;
begin
   Ada.Text_IO.Put_Line ("Euler 075:");
   for L in 1 .. 1_500_000 loop
      if Count_Triangles (L) = 1 then
         Count := Count + 1;
      end if;
      if L mod 1000 = 0 then
         Ada.Text_IO.Put_Line ("L:" & Integer'Image (L) & ", Count:" & Integer'Image (Count));
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Result:" & Integer'Image (Count));
end Euler075;
