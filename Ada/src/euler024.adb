--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Permutations;
procedure Euler024 is
   type Data_Type is range 0 .. 9;
   package Data_Permutations is new Permutations
     (Element_Type => Data_Type);
   My_Permutation : Data_Permutations.Permutation := (0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
   Perm_Count : Natural := 1;
begin
   Data_Permutations.Reset (My_Permutation);
   Ada.Text_IO.Put_Line ("Euler 24:");
   for I in My_Permutation'Range loop
      Ada.Text_IO.Put (Data_Type'Image (My_Permutation (I)) & ",");
   end loop;
   Ada.Text_IO.New_Line;
   begin
      while Perm_Count < 1_000_000 loop
         Data_Permutations.Next (My_Permutation);
         Perm_Count := Perm_Count + 1;
      end loop;
   exception
      when Data_Permutations.Last_Permutation =>
         null;
   end;
   for I in My_Permutation'Range loop
      Ada.Text_IO.Put (Data_Type'Image (My_Permutation (I)) & ",");
   end loop;
   Ada.Text_IO.New_Line;
end Euler024;
