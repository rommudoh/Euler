--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler028 is
   Sum           : Natural := 1;
   Current_Field : Natural := 1;
   Current_Step  : Natural := 2;
begin
   loop
      Current_Field := Current_Field + Current_Step;
      Sum := Sum + Current_Field;
      Current_Field := Current_Field + Current_Step;
      Sum := Sum + Current_Field;
      Current_Field := Current_Field + Current_Step;
      Sum := Sum + Current_Field;
      Current_Field := Current_Field + Current_Step;
      Sum := Sum + Current_Field;
      Current_Step := Current_Step + 2;
      exit when Current_Field >= 1001**2;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 28:" & Integer'Image (Sum));
end Euler028;
