--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Ada.Strings.Fixed;
with Big_Number;
procedure Euler065 is
   package My_Bigs is new Big_Number (100);
   --type Number is range 0 .. 2**63 - 1;
   use My_Bigs;
   function Get_Nth_Euler (N : Positive) return Natural;
   function Get_Nth_Euler (N : Positive) return Natural is
      K : constant Natural := (N + 1) / 3;
   begin
      if N = 1 then
         return 2;
      end if;
      if N mod 3 = 0 then
         return K * 2;
      else
         return 1;
      end if;
   end Get_Nth_Euler;
begin
   Ada.Text_IO.Put_Line ("Euler 065:");
   declare
      Nominator   : Number := To_Big_Number (Get_Nth_Euler (100));
      Denominator : Number := To_Big_Number (1);
      Temp        : Number;
   begin
      for I in reverse 1 .. 99 loop
         -- invert
         Temp := Nominator;
         Nominator := Denominator;
         Denominator := Temp;
         -- add next
         Nominator := Nominator + Denominator * Get_Nth_Euler (I);
      end loop;
      Ada.Text_IO.Put_Line ("Foo:" & Image (Nominator) & " /" & Image (Denominator));
      Ada.Text_IO.Put_Line ("Sum of Nominator Digits:");
      declare
         Text : constant String := Ada.Strings.Fixed.Trim (Image (Nominator), Ada.Strings.Both);
         Sum  : Natural := 0;
      begin
         for I in Text'Range loop
            Sum := Sum + Character'Pos (Text (I)) - Character'Pos ('0');
         end loop;
         Ada.Text_IO.Put_Line (Natural'Image (Sum));
      end;
   end;
end Euler065;
