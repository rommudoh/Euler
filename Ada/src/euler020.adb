--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Big_Number;
procedure Euler020 is
   package My_Big_Numbers is new Big_Number (Max_Digit_Count => 100);
   use My_Big_Numbers;
   function Sum_Of_Digits (N : Number) return Natural is
      Sum : Natural := 0;
      Text : constant String := Image (N);
   begin
      for I in Text'Range loop
         if Text (I) /= ' ' then
            Sum := Sum + Character'Pos (Text (I)) - Character'Pos ('0');
         end if;
      end loop;
      return Sum;
   end Sum_Of_Digits;
   The_Number : Number := To_Big_Number (1);
begin
   for I in 1 .. 100 loop
      The_Number := The_Number * I;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 20:" & Integer'Image (Sum_Of_Digits (The_Number)));
end Euler020;
