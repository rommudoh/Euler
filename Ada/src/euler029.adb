--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Big_Number;
with Ada.Text_IO;
procedure Euler029 is
   package My_Big_Numbers is new Big_Number (100);
   use type My_Big_Numbers.Number;
   package Big_Vectors is new Ada.Containers.Vectors
     (Element_Type => My_Big_Numbers.Number, Index_Type => Positive);
   Potence : My_Big_Numbers.Number;
   Terms : Big_Vectors.Vector;
begin
   for A in 2 .. 100 loop
      for B in 2 .. 100 loop
         Potence := My_Big_Numbers.To_Big_Number (A) ** B;
--         Ada.Text_IO.Put_Line (Integer'Image (A) & " **" & Integer'Image (B) & " =" & My_Big_Numbers.Image (Potence));
         if not Terms.Contains (Potence) then
            Terms.Append (Potence);
         end if;
      end loop;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 29:" & Integer'Image (Integer (Terms.Length)));
end Euler029;
