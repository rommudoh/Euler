--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler120 is
   type Number is range 0 .. 2**63 - 1;
   function Max_R (A : Number) return Number is
      function Modular_Power (Base, Exponent, Modulus : Number) return Number is
         The_Exponent : Number := Exponent mod Modulus;
         The_Base     : Number := Base mod Modulus;
         Result       : Number := 1;
      begin
         while The_Exponent > 0 loop
            if The_Exponent mod 2 = 1 then
               Result := (Result * The_Base) mod Modulus;
            end if;
            The_Exponent := The_Exponent / 2;
            The_Base := (The_Base ** 2) mod Modulus;
         end loop;
         return Result;
      end Modular_Power;
      function R (A, N : Number) return Number is
         Result : Number;
      begin
         Result := Modular_Power (A - 1, N, A ** 2);
         Result := Result + Modular_Power (A + 1, N, A ** 2);
         Result := Result mod A ** 2;
         return Result;
      end R;
      N : Number := A ** 2 / 2;
      Max_R : Number := 0;
      The_R : Number;
   begin
      while N < A ** 2 - 1 loop
         The_R := R (A, N);
         if The_R > Max_R then
            Max_R := The_R;
         end if;
         N := N + 1;
      end loop;
      return Max_R;
   end Max_R;

   Sum : Number := 0;
begin
   Ada.Text_IO.Put_Line ("Euler120:");
   for A in Number range 3 .. 1000 loop
      declare
         Current : Number;
      begin
         Current := Max_R (A);
         if A mod 100 = 0 then
            Ada.Text_IO.Put_Line ("Max_R for" & Number'Image (A) & " =" & Number'Image (Current));
         end if;
         Sum := Sum + Current;
      end;
   end loop;
   Ada.Text_IO.Put_Line ("Sum:" & Number'Image (Sum));
end Euler120;
