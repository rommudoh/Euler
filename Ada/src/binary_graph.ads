--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Finalization;
package Binary_Graph is
   type Natural_Array is array (Positive range <>) of Natural;
   type Connection is record
      From : Positive;
      Left, Right : Positive;
   end record;
   type Connection_Array is array (Positive range <>) of Connection;
   type Graph (Element_Count : Positive) is new Ada.Finalization.Limited_Controlled with private;
   overriding procedure Finalize (Object : in out Graph);
   overriding procedure Initialize (Object : in out Graph);
   procedure Insert (To : in out Graph; Value : Natural; ID : out Positive);
   procedure Insert (To : in out Graph; Value : Natural);
   procedure Insert (To : in out Graph; Values : Natural_Array);
   procedure Connect (On : in out Graph; From, Left, Right : Positive);
   procedure Connect (On : in out Graph; Connections : Connection_Array);
   Graph_Full : exception;
   No_Connections_Free : exception;
   Out_Of_Bounds : exception;
private
   type Node;
   type Node_Access is access Node;
   type Node is record
      Value       : Natural;
      Left, Right : Node_Access;
   end record;
   type Nodes is array (Positive range <>) of Node_Access;
   type Graph (Element_Count : Positive) is new Ada.Finalization.Limited_Controlled with record
      All_Nodes  : Nodes (1 .. Element_Count) := (others => null);
      Last_Index : Natural := 0;
   end record;
end Binary_Graph;
