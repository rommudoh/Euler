--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler070 is
   function Is_Permutation (A, B : Positive) return Boolean is
      Counts : array (0 .. 9) of Integer := (others => 0);
      Value  : Natural := A;
      Mod_Value : Natural;
   begin
      while Value /= 0 loop
         Mod_Value := Value mod 10;
         Counts (Mod_Value) := Counts (Mod_Value) + 1;
         Value := Value / 10;
      end loop;
      Value := B;
      while Value /= 0 loop
         Mod_Value := Value mod 10;
         if Counts (Mod_Value) = 0 then
            return False;
         end if;
         Counts (Mod_Value) := Counts (Mod_Value) - 1;
         Value := Value / 10;
      end loop;
      return True;
   end Is_Permutation;

   function φ (N : Positive) return Positive is
      Value : Positive := N;
      Result : Positive := N;
      I : Positive := 2;
   begin
      while I * I <= Value loop
         if Value mod I = 0 then
            Result := Result - Result / I;
         end if;
         while Value mod I = 0 loop
            Value := Value / I;
         end loop;
         I := I + 1;
      end loop;
      if Value > 1 then
         Result := Result - Result / Value;
      end if;
      return Result;
   end φ;

   Quotient : Long_Float;
   Phi : Positive;
   Min_Quotient : Long_Float := Long_Float'Last;
   Min_N  : Positive;
begin
   Ada.Text_IO.Put_Line ("Euler070:");
   for I in 2 .. 10 ** 7 loop
      Phi := φ (I);
      Quotient := Long_Float (I) / Long_Float (Phi);
      if Is_Permutation (I, Phi) and then Quotient < Min_Quotient then
         Min_Quotient := Quotient;
         Min_N := I;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Min Quotient:" & Long_Float'Image (Min_Quotient));
   Ada.Text_IO.Put_Line ("Min N:" & Integer'Image (Min_N));
   Ada.Text_IO.Put_Line ("Phi N:" & Integer'Image (φ (Min_N)));
end Euler070;
