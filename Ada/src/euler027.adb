--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Ada.Text_IO;
with Primes;
procedure Euler027 is
   package Natural_Primes is new Primes (Number_Type   => Natural,
                                         Maximum_Prime => 14_000);
   package Prime_Vectors is new Ada.Containers.Vectors
     (Element_Type => Integer, Index_Type => Positive);
   Primes_To_Test : Prime_Vectors.Vector;
   Maximum_Prime_Count : Natural := 0;
   Maximum_A : Integer := 0;
   Maximum_B : Natural := 0;
begin
   -- B can only be prime!
   for N in 1 .. 1000 loop
      declare
         Prime : constant Integer := Natural_Primes.Get_Next_Prime;
      begin
         exit when Prime > 1000;
         -- B can only be positive!
         Primes_To_Test.Append (Prime);
      end;
   end loop;
   -- A can be -1000 .. 1000
   for A in -1000 .. 1000 loop
      for B in Primes_To_Test.First_Index .. Primes_To_Test.Last_Index loop
         declare
            Testant     : Integer;
            N           : Natural := 0;
         begin
            loop
               Testant := N ** 2 + A * N + Primes_To_Test.Element (B);
               exit when Testant <= 0 or else not Natural_Primes.Is_Prime (Testant);
               N := N + 1;
            end loop;
            -- N : Primes generated
            if N > Maximum_Prime_Count then
               Maximum_Prime_Count := N;
               Maximum_B := Primes_To_Test.Element (B);
               Maximum_A := A;
            end if;
         end;
      end loop;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 28: A =" & Integer'Image (Maximum_A) & ", B =" & Integer'Image (Maximum_B) & " produced" & Integer'Image (Maximum_Prime_Count) & " primes.");
   Ada.Text_IO.Put_Line ("Product A*B =" & Integer'Image (Maximum_A * Maximum_B));
   Ada.Text_IO.Put_Line ("Last Prime  =" & Integer'Image (Natural_Primes.Get_Last_Prime));
end Euler027;
