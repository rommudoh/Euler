--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Unchecked_Deallocation;
package body Triangular is
   overriding procedure Initialize (Object : in out Pyramid) is
   begin
      Object.Rows := null;
   end Initialize;
   overriding procedure Finalize (Object : in out Pyramid) is
      procedure Free is new Ada.Unchecked_Deallocation
        (Object => Row, Name => Row_Access);
      procedure Free is new Ada.Unchecked_Deallocation
        (Object => Row_Access_Array, Name => Row_Access_Array_Access);
   begin
      if Object.Rows /= null then
         for N in Object.Rows'Range loop
            if Object.Rows (N) /= null then
               Free (Object.Rows (N));
            end if;
         end loop;
         Free (Object.Rows);
      end if;
   end Finalize;
   procedure Create_Pyramid (Item : in out Pyramid; Height : Positive) is
   begin
      if Item.Rows /= null then
         Item.Finalize;
      end if;
      Item.Rows := new Row_Access_Array (1 .. Height);
      for N in Item.Rows'Range loop
         Item.Rows (N) := new Row (1 .. N);
      end loop;
   end Create_Pyramid;
   function Row_Count (Item : Pyramid) return Positive is
   begin
      return Item.Rows'Length;
   end Row_Count;
   function Column_Count (Item : Pyramid; Row : Positive) return Positive is
   begin
      return Item.Rows (Row)'Length;
   end Column_Count;
   procedure Set (Item : in out Pyramid; Row, Column : Positive; Value : Natural) is
   begin
      Item.Rows (Row) (Column) := Value;
   end Set;
   function Get (Item : Pyramid; Row, Column : Positive) return Natural is
   begin
      return Item.Rows (Row) (Column);
   end Get;
end Triangular;
