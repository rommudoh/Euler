--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler014 is
   type Number is mod 2 ** 64;
   function Next_N (Source : Number) return Number is
   begin
      if Source mod 2 = 0 then
         return Source / 2;
      else
         return 3 * Source + 1;
      end if;
   end Next_N;
   function Count_Chain (Start : Number) return Positive is
      Current : Number := Start;
      Count  : Positive := 1;
   begin
      while Current /= 1 loop
         Current := Next_N (Current);
         Count := Count + 1;
      end loop;
      return Count;
   end Count_Chain;
   Longest_Chain : Positive := 1;
   Longest_Chain_Producer : Number := 0;
begin
   for X in Number range 2 .. 999_999 loop
      declare
         Chain_Length : constant Positive := Count_Chain (X);
      begin
         if Chain_Length > Longest_Chain then
            Longest_Chain := Chain_Length;
            Longest_Chain_Producer := X;
         end if;
      end;
   end loop;
   Ada.Text_IO.Put_Line ("Longest chain length:" & Positive'Image (Longest_Chain));
   Ada.Text_IO.Put_Line ("Longest chain producer:" & Number'Image (Longest_Chain_Producer));
end Euler014;
