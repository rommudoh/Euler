--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Calendar.Formatting;
with Ada.Text_IO;
procedure Euler019 is
   use type Ada.Calendar.Formatting.Day_Name;
   Time  : Ada.Calendar.Time;
   Count : Natural := 0;
begin
   for Year in Ada.Calendar.Year_Number range 1901 .. 2000 loop
      for Month in Ada.Calendar.Month_Number'Range loop
         Time := Ada.Calendar.Time_Of (Year    => Year,
                                       Month   => Month,
                                       Day     => 1);
         if Ada.Calendar.Formatting.Day_Of_Week (Time) = Ada.Calendar.Formatting.Sunday then
            Count := Count + 1;
         end if;
      end loop;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 19:" & Integer'Image (Count));
end Euler019;
