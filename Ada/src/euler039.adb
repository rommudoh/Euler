--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler039 is
   function Count_Triangles (Perimeter : Positive) return Natural is
      A : Natural;
      Count : Natural := 0;
   begin
      for C in 1 .. Perimeter / 2 loop
         for B in 1 .. (Perimeter - C) / 2 loop
            -- calculate A
            A := Perimeter - C - B;
            if A * A + B * B = C * C then
               Count := Count + 1;
            end if;
         end loop;
      end loop;
      return Count;
   end Count_Triangles;
   Max_P : Natural := 0;
   Max_Count : Natural := 0;
   Count : Natural;
begin
   Ada.Text_IO.Put_Line ("Euler039:");
   for P in 1 .. 1000 loop
      Count := Count_Triangles (P);
      if Count > Max_Count then
         Max_Count := Count;
         Max_P := P;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("P:" & Integer'Image (Max_P));
   Ada.Text_IO.Put_Line ("Count:" & Integer'Image (Max_Count));
end Euler039;
