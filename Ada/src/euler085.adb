--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler085 is
   function Count_Sub_Rects (Height, Width : Natural) return Natural is
      Sum : Natural := 0;
   begin
      -- 1x1 rects
--      Sum := Sum + Height * Width;
      -- 1 height, 2..width width
      for H in 1 .. Height loop
         for W in 1 .. Width loop
            Sum := Sum + (Height - H + 1) * (Width - W + 1);
         end loop;
      end loop;
      return Sum;
   end Count_Sub_Rects;
   Max_Sum : Natural := 0;
   Max_X, Max_Y : Natural := 0;
begin
   Ada.Text_IO.Put_Line ("Euler 085:");
   for X in 1 .. 2_000 loop
      for Y in 1 .. 2_000_000 / X loop
         declare
            Sum : constant Natural := Count_Sub_Rects (X, Y);
         begin
            exit when Sum > 2_000_000;
            if Sum < 2_000_000 and then Sum > Max_Sum then
               Max_Sum := Sum;
               Max_X := X;
               Max_Y := Y;
            end if;
         end;
      end loop;
      Ada.Text_IO.Put_Line ("X =" & Integer'Image (X) & ", max:" & Integer'Image (Max_Sum));
   end loop;

   Ada.Text_IO.Put_Line (Integer'Image (Max_X) & " x" & Integer'Image (Max_Y) & " =" & Integer'Image (Max_X * Max_Y) & ", sum:" & Integer'Image (Max_Sum));
end Euler085;
