--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler045 is
   type Number is mod 2 ** 64;
   function Triangle_Number (N : Positive) return Number is
   begin
      return Number (N) * (Number (N) + 1) / 2;
   end Triangle_Number;
   function Pentagonal_Number (N : Positive) return Number is
   begin
      return Number (N) * (3 * Number (N) - 1) / 2;
   end Pentagonal_Number;
   function Hexagonal_Number (N : Positive) return Number is
   begin
      return Number (N) * (2 * Number (N) - 1);
   end Hexagonal_Number;
   Triangle_Index : Positive := 285;
   Pentagonal_Index : Positive := 165;
   Hexagonal_Index : Positive := 143;
   Triangle : Number := Triangle_Number (Triangle_Index);
   Pentagonal : Number := Pentagonal_Number (Pentagonal_Index);
   Hexagonal : Number := Hexagonal_Number (Hexagonal_Index);
begin
   Ada.Text_IO.Put_Line ("Number:" & Number'Image (Triangle) & "," & Number'Image (Pentagonal) & "," & Number'Image (Hexagonal));
   -- make it off
   Triangle_Index := Triangle_Index + 1;
   Triangle := Triangle_Number (Triangle_Index);
   loop
      while Hexagonal < Triangle loop
         Hexagonal_Index := Hexagonal_Index + 1;
         Hexagonal := Hexagonal_Number (Hexagonal_Index);
      end loop;
      while Pentagonal < Hexagonal loop
         Pentagonal_Index := Pentagonal_Index + 1;
         Pentagonal := Pentagonal_Number (Pentagonal_Index);
      end loop;
      while Triangle < Pentagonal loop
         Triangle_Index := Triangle_Index + 1;
         Triangle := Triangle_Number (Triangle_Index);
      end loop;
      exit when Triangle = Pentagonal and then Pentagonal = Hexagonal and then Hexagonal = Triangle;
   end loop;
   Ada.Text_IO.Put_Line ("Number:" & Number'Image (Triangle) & "," & Number'Image (Pentagonal) & "," & Number'Image (Hexagonal));
end Euler045;
