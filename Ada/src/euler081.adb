--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Vectors;
with Ada.Strings.Fixed;
with Ada.Text_IO;
procedure Euler081 is
   type Matrix is array (Positive range <>, Positive range <>) of Natural;
   type Coordinate is record
      X, Y : Positive;
   end record;
   package Coordinates is new Ada.Containers.Vectors
     (Element_Type => Coordinate, Index_Type => Positive);
   type Node is record
      Position : Coordinate;
      Value    : Natural;
   end record;
   function "<" (Left, Right : Node) return Boolean is
   begin
      if Left.Value < Right.Value then
         return True;
      elsif Left.Value > Right.Value then
         return False;
      elsif Left.Position.X < Right.Position.X then
         return True;
      elsif Left.Position.X > Right.Position.X then
         return False;
      else
         return Left.Position.Y < Right.Position.Y;
      end if;
   end "<";
   overriding function "=" (Left, Right : Node) return Boolean is
   begin
      return Left.Position = Right.Position;
   end "=";
   package Nodes is new Ada.Containers.Vectors
     (Element_Type => Node, Index_Type => Positive);
   package Node_Sort is new Nodes.Generic_Sorting;
   function Get_Neighbours (On : Matrix; From : Coordinate) return Coordinates.Vector is
      Result : Coordinates.Vector;
   begin
      if From.X < On'Last (1) then
         Result.Append ((X => From.X + 1, Y => From.Y));
      end if;
      if From.Y < On'Last (2) then
         Result.Append ((X => From.X, Y => From.Y + 1));
      end if;
      return Result;
   end Get_Neighbours;
   function A_Star (Costs : Matrix; Source : Coordinate; Target : Coordinate) return Natural is
      Closed_Set : Nodes.Vector;
      Open_Set   : Nodes.Vector;
   begin
      -- insert first coordinate
      Open_Set.Append (New_Item => (Position => Source, Value => Costs (Source.X, Source.Y)));
      -- open set empty => target not reachable
      while not Open_Set.Is_Empty loop
         -- get coordinate with lowest costs
         Node_Sort.Sort (Open_Set);
         declare
            Current : constant Node := Open_Set.First_Element;
         begin
            -- target reched, return costs
            if Current.Position = Target then
               return Current.Value;
            end if;
            -- move coordinate to closed set
            Open_Set.Delete_First;
            Closed_Set.Append (New_Item => Current);
            -- add all neighbours to open set
            declare
               use type Coordinates.Cursor;
               use type Nodes.Cursor;
               Neighbours : constant Coordinates.Vector := Get_Neighbours (Costs, Current.Position);
               Next   : Coordinates.Cursor := Neighbours.First;
               Next_Key   : Node;
               Insert_Cursor : Nodes.Cursor;
            begin
               while Next /= Coordinates.No_Element loop
                  Next_Key.Position := Coordinates.Element (Next);
                  -- closed = already reached through shorter path
                  if not Closed_Set.Contains (Next_Key) then
                     -- new costs = Current_Costs + Costs (X,Y)
                     Next_Key.Value := Current.Value + Costs (Next_Key.Position.X, Next_Key.Position.Y);
                     Insert_Cursor := Open_Set.Find (Next_Key);
                     if Insert_Cursor = Nodes.No_Element then
                        Open_Set.Append (New_Item => Next_Key);
                     elsif Next_Key.Value < Nodes.Element (Insert_Cursor).Value then
                        Open_Set.Replace_Element (Position => Insert_Cursor, New_Item => Next_Key);
                     end if;
                  end if;
                  Coordinates.Next (Next);
               end loop;
            end;
         end;
      end loop;
      return 0;
   end A_Star;
   Test_Matrix : constant Matrix := ((131, 673, 234, 103, 18),
                                     (201, 96, 342, 965, 150),
                                     (630, 803, 746, 422, 111),
                                     (537, 699, 497, 121, 956),
                                     (805, 732, 524, 37, 331));
   Big_Matrix : Matrix (1 .. 80, 1 .. 80);
begin
   Ada.Text_IO.Put_Line ("Euler081:");
   Ada.Text_IO.Put_Line ("Min Path:" & Integer'Image (A_Star (Test_Matrix, (X => 1, Y => 1), (X => 5, Y => 5))));
   declare
      File : Ada.Text_IO.File_Type;
   begin
      Ada.Text_IO.Open (File => File,
                        Mode => Ada.Text_IO.In_File,
                        Name => "matrix.txt");
      for Y in Big_Matrix'Range (2) loop
         declare
            Line        : constant String := Ada.Text_IO.Get_Line (File);
            First, Last : Natural := Line'First;
            X           : Positive := Big_Matrix'First (1);
         begin
            loop
               Last := Ada.Strings.Fixed.Index (Source  => Line,
                                                Pattern => ",",
                                                From    => First);
               if Last = 0 then
                  Last := Line'Last;
               end if;
               Big_Matrix (X, Y) := Natural'Value (Line (First .. Last - 1));
               First := Last + 1;
               X := X + 1;
               exit when X > Big_Matrix'Last (1);
            end loop;
         end;
      end loop;
      Ada.Text_IO.Close (File);
   end;
   Ada.Text_IO.Put_Line ("Min Path:" & Integer'Image
     (A_Star
        (Costs  => Big_Matrix,
         Source => (X => 1, Y => 1),
         Target => (X => 80, Y => 80))));
end Euler081;
