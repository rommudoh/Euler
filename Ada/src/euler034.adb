--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler034 is
   Faculties : array (0 .. 9) of Natural := (0 => 1, 1 => 1, 2 => 2, others => 0);
   function Faculty (N : Natural) return Natural is
   begin
      if Faculties (N) /= 0 then
         return Faculties (N);
      end if;
      Faculties (N) := Faculty (N - 1) * N;
      return Faculties (N);
   end Faculty;
   function Sum_Of_Factorials (N : Natural) return Natural is
      Value : Natural := N;
      Result : Natural := 0;
   begin
      while Value > 0 loop
         Result := Result + Faculty (Value mod 10);
         Value := Value / 10;
      end loop;
      return Result;
   end Sum_Of_Factorials;
   X         : Natural := 3;
   Sum       : Natural := 0;
begin
   Ada.Text_IO.Put_Line ("Euler 34:");
   while X < Faculty (9) * 9 loop
      if X = Sum_Of_Factorials (X) then
         Sum := Sum + X;
      end if;
      X := X + 1;
   end loop;
   Ada.Text_IO.Put_Line (Integer'Image (Sum));
end Euler034;
