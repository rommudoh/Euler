--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Indefinite_Vectors;
with Ada.Containers.Vectors;
with Ada.Strings.Fixed;
with Ada.Text_IO;
procedure Euler042 is
   package String_Vectors is new Ada.Containers.Indefinite_Vectors
     (Element_Type => String, Index_Type => Positive);

   type Number is mod 2 ** 64;
   package Number_Vectors is new Ada.Containers.Vectors
     (Element_Type => Number, Index_Type => Positive);

   Triangles : Number_Vectors.Vector;
   function Triangle_Number (N : Positive) return Number is
   begin
      return Number (N) * (Number (N) + 1) / 2;
   end Triangle_Number;
   function Is_Triangle_Number (N : Number) return Boolean is
      use type Ada.Containers.Count_Type;
   begin
      while Triangles.Length = 0 or else N > Triangles.Last_Element loop
         Triangles.Append (Triangle_Number (Natural (Triangles.Last_Index) + 1));
      end loop;
      return Triangles.Contains (N);
   end Is_Triangle_Number;

   function Calculate_Value (S : String) return Number is
      Result : Number := 0;
   begin
      for I in S'Range loop
         Result := Result + (Character'Pos (S (I)) - Character'Pos ('A') + 1);
      end loop;
      return Result;
   end Calculate_Value;

   File : Ada.Text_IO.File_Type;
   Words : String_Vectors.Vector;
   Sum : Natural := 0;
begin
   -- open file
   Ada.Text_IO.Open (File => File,
                     Mode => Ada.Text_IO.In_File,
                     Name => "words.txt");
   -- read content (only one line)
   declare
      Whole_Line : constant String := Ada.Text_IO.Get_Line (File => File);
      From, To   : Natural := Whole_Line'First;
      Done       : Boolean := False;
   begin
      -- close file
      Ada.Text_IO.Close (File => File);
      -- split by "," and add names to list
      while not Done loop
         To := Ada.Strings.Fixed.Index (Source  => Whole_Line,
                                              Pattern => ",",
                                        From    => From);
         -- last name has no "," afterwards, fix it
         if To < 1 then
            To := Whole_Line'Last + 1;
            Done := True;
         end if;
         Words.Append (New_Item => Whole_Line (From + 1 .. To - 2));
         From := To + 1;
      end loop;
   end;
   -- check words
   for I in Words.First_Index .. Words.Last_Index loop
      if Is_Triangle_Number (Calculate_Value (Words.Element (I))) then
         Sum := Sum + 1;
      end if;
   end loop;

   Ada.Text_IO.Put_Line ("Number:" & Integer'Image (Sum));
end Euler042;
