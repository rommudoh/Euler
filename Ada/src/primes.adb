--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

package body Primes is
   type Bool_Array_Access is access Bool_Array;

   Primes        : constant Bool_Array_Access := new Bool_Array (1 .. Maximum_Prime);
   Last_Prime    : Number_Type := 1;

   function Get_Last_Prime return Number_Type is
   begin
      return Last_Prime;
   end Get_Last_Prime;

   function Get_Next_Prime return Number_Type is
      Next : Number_Type := Last_Prime + 1;
   begin
      if Last_Prime = 1 then
         for I in Primes'Range loop
            Primes (I) := True;
         end loop;
         Primes (1) := False;
      end if;
      begin
         while not Primes (Next) loop
            Next := Next + 1;
         end loop;
      exception
         when Constraint_Error => return Last_Prime;
      end;
      declare
         Remove : Number_Type := Next;
      begin
         loop
            Remove := Remove + Next;
            exit when Remove > Primes'Last;
            Primes (Remove) := False;
         end loop;
      end;
      Last_Prime := Next;
      return Last_Prime;
   end Get_Next_Prime;

   function Is_Prime (Number : Number_Type) return Boolean is
   begin
      while Last_Prime < Number loop
         declare
            Prime : constant Number_Type := Get_Next_Prime;
            pragma Unreferenced (Prime);
         begin
            null;
         end;
      end loop;
      return Primes (Number);
   end Is_Prime;

   function Is_Truncable_Prime (Number : Number_Type) return Boolean is
      Value : Number_Type := Number;
   begin
      -- truncable from right
      while Value > 0 loop
         if not Is_Prime (Value) then
            return False;
         end if;
         Value := Value / 10;
      end loop;
      -- truncable from left
      Value := 10;
      while Value < Number loop
         if not Is_Prime (Number mod Value) then
            return False;
         end if;
         Value := Value * 10;
      end loop;
      return True;
   end Is_Truncable_Prime;

   function Get_Primes return Bool_Array is
   begin
      return Primes.all (1 .. Last_Prime);
   end Get_Primes;
begin
   Primes.all := (others => True);
--     for I in Primes'Range loop
--        Primes (I) := True;
--     end loop;
   Primes (1) := False;
end Primes;
