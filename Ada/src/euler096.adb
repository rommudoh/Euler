--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler096 is
   type Symbol is range 0 .. 9;
   subtype Valid_Symbol is Symbol range
      Symbol'Succ (Symbol'First) .. Symbol'Last;

   Valid_Symbol_Count : constant Positive :=
      Valid_Symbol'Pos (Valid_Symbol'Last) -
      Valid_Symbol'Pos (Valid_Symbol'First) +
      1;
   Unknown            : constant Symbol   := 0;

   type Grid is
     array (1 .. Valid_Symbol_Count, 1 .. Valid_Symbol_Count) of Symbol;
   type Symbols is array (Positive range <>) of Symbol;
   type Possible_Symbols is array (Symbol) of Boolean;

   function Get_Possible_Symbols
     (Item        : Grid;
      Row, Column : Positive)
      return        Symbols
   is
      Temporary : Possible_Symbols := (others => True);
      Count     : Natural          := 0;
   begin
      for Z in Item'Range (1) loop
         Temporary (Item (Z, Column))  := False;
      end loop;
      for Z in Item'Range (2) loop
         Temporary (Item (Row, Z))  := False;
      end loop;
      for I in ((Row - 1) / 3) * 3 + 1 .. ((Row - 1) / 3 + 1) * 3 loop
         for J in
               ((Column - 1) / 3) * 3 + 1 ..
               ((Column - 1) / 3 + 1) * 3
         loop
            Temporary (Item (I, J))  := False;
         end loop;
      end loop;
      for Z in Temporary'Range loop
         if Z /= Unknown and then Temporary (Z) then
            Count := Count + 1;
         end if;
      end loop;
      declare
         Result : Symbols (1 .. Count);
         Last   : Positive := Result'First;
      begin
         for Z in Temporary'Range loop
            if Z /= Unknown and then Temporary (Z) then
               Result (Last) := Z;
               Last          := Last + 1;
            end if;
         end loop;
         return Result;
      end;
   end Get_Possible_Symbols;

   function Solve (Item : Grid) return Grid is
      type Bool_Grid is array (Item'Range (1), Item'Range (2)) of Boolean;
      Result : Grid := Item;
      Unsolvable : exception;
      Unknowns : Bool_Grid := (others => (others => False));
   begin
      -- first fill all fields with only one possibility
      for Row in Result'Range (1) loop
         for Column in Result'Range (2) loop
            -- only if field is unknown
            if Result (Row, Column) = Unknown then
               declare
                  Possibilities : constant Symbols :=
                     Get_Possible_Symbols
                       (Item   => Result,
                        Row    => Row,
                        Column => Column);
               begin
                  -- fill in single value
                  if Possibilities'Length = 1 then
                     Result (Row, Column) :=
                       Possibilities (Possibilities'First);
                  elsif Possibilities'Length = 0 then
                     -- no solution possible!
                     raise Unsolvable;
                  else
                     -- remember coordinate for later
                     Unknowns (Row, Column) := True;
                  end if;
               end;
            end if;
         end loop;
      end loop;
      -- redo the unknown ones
      for Row in Result'Range (1) loop
         for Column in Result'Range (2) loop
            -- if coordinate was remembered earlier
            if Unknowns (Row, Column) then
               declare
                  -- recalculate possibilities
                  Possibilities : constant Symbols := Get_Possible_Symbols (Item   => Result,
                                                                            Row    => Row,
                                                                            Column => Column);
               begin
                  -- no solution possible!
                  if Possibilities'Length = 0 then
                     raise Unsolvable;
                  else
                     -- try out all possibilities
                     for I in Possibilities'Range loop
                        begin
                           Result (Row, Column) := Possibilities (I);
                           return Solve (Result);
                        exception
                           when Unsolvable =>
                              null;
                        end;
                     end loop;
                  end if;
               end;
            end if;
         end loop;
      end loop;
      return Result;
   end Solve;

   procedure Print (Item : Grid) is
   begin
      Ada.Text_IO.Put_Line (" +-------+-------+-------+");
      for Row in Item'Range (1) loop
         Ada.Text_IO.Put (" |");
         for Column in Item'Range (2) loop
            Ada.Text_IO.Put (Symbol'Image (Item (Row, Column)));
            if Column mod 3 = 0 then
               Ada.Text_IO.Put (" |");
            end if;
         end loop;
         Ada.Text_IO.New_Line;
         if Row mod 3 = 0 then
            Ada.Text_IO.Put_Line (" +-------+-------+-------+");
         end if;
      end loop;
   end Print;

   Count   : Natural := 0;
begin
   Ada.Text_IO.Put_Line ("Euler 096:");
   declare
      File : Ada.Text_IO.File_Type;
   begin
      Ada.Text_IO.Open
        (File => File,
         Mode => Ada.Text_IO.In_File,
         Name => "grids.txt");
      while not Ada.Text_IO.End_Of_File (File) loop
         declare
            Title : constant String := Ada.Text_IO.Get_Line (File);
            Sudoku : Grid;
         begin
            for Row in Sudoku'Range (1) loop
               declare
                  Line : constant String := Ada.Text_IO.Get_Line (File);
               begin
                  for Column in Sudoku'Range (2) loop
                     Sudoku (Row, Column) := Symbol (Character'Pos (Line (Column)) - Character'Pos ('0'));
                  end loop;
               end;
            end loop;
            Ada.Text_IO.Put_Line (Title);
            Sudoku := Solve (Sudoku);
            Print (Sudoku);
            Count := Count + Natural (Sudoku (1, 1)) * 100 + Natural (Sudoku (1, 2)) * 10 + Natural (Sudoku (1, 3));
            Ada.Text_IO.Put_Line ("Count :" & Integer'Image (Count));
         end;
      end loop;
      Ada.Text_IO.Close (File);
   end;
   Ada.Text_IO.Put_Line ("Result:" & Integer'Image (Count));
end Euler096;
