--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler069 is
   function φ (N : Positive) return Positive is
      Value : Positive := N;
      Result : Positive := N;
      I : Positive := 2;
   begin
      while I * I <= Value loop
         if Value mod I = 0 then
            Result := Result - Result / I;
         end if;
         while Value mod I = 0 loop
            Value := Value / I;
         end loop;
         I := I + 1;
      end loop;
      if Value > 1 then
         Result := Result - Result / Value;
      end if;
      return Result;
   end φ;

   Quotient : Long_Float;
   Phi : Positive;
   Max_Quotient : Long_Float := 0.0;
   Max_N  : Positive;
begin
   Ada.Text_IO.Put_Line ("Euler069:");
   for I in 2 .. 1_000_000 loop
      Phi := φ (I);
      Quotient := Long_Float (I) / Long_Float (Phi);
      if Quotient > Max_Quotient then
         Max_Quotient := Quotient;
         Max_N := I;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Max Quotient:" & Long_Float'Image (Max_Quotient));
   Ada.Text_IO.Put_Line ("Max N:" & Integer'Image (Max_N));
end Euler069;
