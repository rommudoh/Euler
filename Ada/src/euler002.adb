--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Ada.Containers.Vectors;
procedure Euler002 is
   package Natural_Vectors is new Ada.Containers.Vectors (Element_Type => Natural, Index_Type => Natural);
   use Natural_Vectors;

   -- global fibonacci table
   Fib_Table : Vector;
   Initialized : Boolean := False;

   function Fib (N : Natural) return Natural is
      procedure Initialize is
      begin
         Fib_Table.Clear;
         Fib_Table.Append (1);
         Fib_Table.Append (1);
         Initialized := True;
      end Initialize;
   begin
      if not Initialized then
         Initialize;
      end if;
      if Fib_Table.Last_Index < N then
         Fib_Table.Append (Fib (N - 1) + Fib (N - 2));
      end if;
      return Fib_Table.Element (N);
   end Fib;

   The_Fib : Natural;
   Count   : Natural := 1;
   Sum     : Natural := 0;
begin
   loop
      The_Fib := Fib (Count);
      exit when The_Fib > 4_000_000;
      Count := Count + 1;
      if The_Fib mod 2 = 0 then
         Sum := Sum + The_Fib;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Fibs created:" & Integer'Image (Count) & ", sum is" & Integer'Image (Sum));
end Euler002;
