--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

package body Abundands is
   Maximum_Abundands   : constant := 10000;
   Abundand_Array      : array (1 .. Maximum_Abundands) of Natural := (others => 1);
   Last_Abundand : Natural := 0;
   function Sum_Of_Divisors (N : Natural) return Natural is
      Sum   : Natural := 0;
   begin
      for I in 1 .. N / 2 loop
         if N mod I = 0 then
            Sum := Sum + I;
         end if;
      end loop;
      return Sum;
   end Sum_Of_Divisors;
   function Is_Abundand (N : Natural) return Boolean is
   begin
      if Last_Abundand > 0 and then N <= Abundand_Array (Last_Abundand) then
         for I in 1 .. Last_Abundand loop
            exit when Abundand_Array (I) > N;
            if Abundand_Array (I) = N then
               return True;
            end if;
         end loop;
         return False;
      else
         if Sum_Of_Divisors (N) > N then
            return True;
         else
            return False;
         end if;
      end if;
   end Is_Abundand;
   function Get_Next_Abundand return Natural is
      Result : Natural;
   begin
      if Last_Abundand = 0 then
         -- first call
         Result := 1;
      else
         -- call N + 1
         Result := Abundand_Array (Last_Abundand);
      end if;
      -- continue search
      loop
         Result := Result + 1;
         exit when Is_Abundand (Result);
      end loop;
      Last_Abundand := Last_Abundand + 1;
      Abundand_Array (Last_Abundand) := Result;
      return Result;
   end Get_Next_Abundand;
   function Get_Nth_Abundand (N : Positive) return Natural is
      Temporary : Natural;
      pragma Unreferenced (Temporary);
   begin
      for X in Last_Abundand + 1 .. N loop
         Temporary := Get_Next_Abundand;
      end loop;
      return Abundand_Array (N);
   end Get_Nth_Abundand;
end Abundands;
