--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Primes;
procedure Euler035 is
   package My_Primes is new Primes (Number_Type   => Natural,
                                    Maximum_Prime => 2_000_000);
   function Rotate (Number : Natural; Digit_Count : Natural) return Natural is
      Lead : constant Natural := Number / 10 ** (Digit_Count - 1);
   begin
      return (Number - Lead * 10 ** (Digit_Count - 1)) * 10 + Lead;
   end Rotate;
   function Is_Circular_Prime (Number : Natural) return Boolean is
      Value : Natural := Number;
      Digit_Count : Natural := 0;
   begin
      while Value > 0 loop
         Digit_Count := Digit_Count + 1;
         Value := Value / 10;
      end loop;
      Value := Number;
      for I in 1 .. Digit_Count loop
         if not My_Primes.Is_Prime (Value) then
            return False;
         end if;
         Value := Rotate (Value, Digit_Count);
      end loop;
      return True;
   end Is_Circular_Prime;
   Sum : Natural := 0;
begin
   for N in 2 .. 1_000_000 loop
      if Is_Circular_Prime (N) then
         -- Ada.Text_IO.Put_Line ("Circular Prime:" & Integer'Image (N));
         Sum := Sum + 1;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Primes:" & Integer'Image (Sum));
end Euler035;
