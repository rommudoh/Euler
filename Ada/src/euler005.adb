--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler005 is
   type Number_Array is array (Positive range <>) of Positive;
   function Minimum (Numbers : Number_Array) return Positive is
      Result : Positive := 1;
   begin
      for N in Numbers'Range loop
         if Numbers (N) /= 1 and then (Numbers (N) < Result or else Result = 1) then
            Result := Numbers (N);
         end if;
      end loop;
      return Result;
   end Minimum;
   function Find_Min_Divisor (Numbers : Number_Array) return Positive is
      Result : Positive := 2;
   begin
      for Round in 2 .. Minimum (Numbers) loop
         for N in Numbers'Range loop
            if Numbers (N) mod Result = 0 then
               return Result;
            end if;
         end loop;
         Result := Result + 1;
      end loop;
      return 1;
   end Find_Min_Divisor;
   function Least_Common_Multiple (Numbers : Number_Array) return Positive is
      Last    : Number_Array := Numbers;
      Divisor : Positive := 1;
      Result  : Positive := 1;
   begin
      loop
         Divisor := Find_Min_Divisor (Last);
         exit when Divisor = 1;
         Result := Result * Divisor;
         for N in Last'Range loop
            if Last (N) mod Divisor = 0 then
               Last (N) := Last (N) / Divisor;
            end if;
         end loop;
      end loop;
      return Result;
   end Least_Common_Multiple;
   Test_Array : constant Number_Array := (1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                                          11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
   Test_Value : constant Positive := Least_Common_Multiple (Test_Array);
begin
   Ada.Text_IO.Put_Line ("Least Common Multiple is:" & Integer'Image (Test_Value));
end Euler005;
