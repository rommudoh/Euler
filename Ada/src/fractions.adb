--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

package body Fractions is
   procedure Normalize (X : in out Fraction);
   function Normalize (X : Fraction) return Fraction;

   function "*" (Left, Right : Fraction) return Fraction is
   begin
      return Normalize ((Nominator  => Left.Nominator * Right.Nominator,
                         Denominator => Left.Denominator * Right.Denominator,
                         Negative    => Left.Negative xor Right.Negative));
   end "*";

   function "*" (Left : Fraction; Right : Integer) return Fraction is
   begin
      return Normalize ((Nominator  => Left.Nominator * abs (Right),
                         Denominator => Left.Denominator,
                         Negative    => Left.Negative xor (Right < 0)));
   end "*";

   function "*" (Left : Integer; Right : Fraction) return Fraction is
   begin
      return Right * Left;
   end "*";

   function "+" (Left, Right : Fraction) return Fraction is
   begin
      if Left.Negative then
         return Right - (-Left);
      elsif Right.Negative then
         return Left - (-Right);
      end if;
      return Normalize ((Nominator  =>
                           Left.Nominator * Right.Denominator +
                             Right.Nominator * Left.Denominator,
                         Denominator => Left.Denominator * Right.Denominator,
                         Negative    => False));
   end "+";

   function "+" (Left : Fraction; Right : Integer) return Fraction is
   begin
      if Left.Negative then
         return Right - (-Left);
      elsif Right < 0 then
         return Left - (-Right);
      end if;
      return Normalize ((Nominator  => Left.Nominator + Right * Left.Denominator,
                         Denominator => Left.Denominator,
                         Negative    => False));
   end "+";

   function "+" (Left : Integer; Right : Fraction) return Fraction is
   begin
      return Right + Left;
   end "+";

   function "-" (X : Fraction) return Fraction is
   begin
      return (Nominator   => X.Nominator,
              Denominator => X.Denominator,
              Negative    => not X.Negative);
   end "-";

   function "-" (Left, Right : Fraction) return Fraction is
   begin
      if Right.Negative then
         return Left + (-Right);
      elsif Left.Negative then
         return -((-Left) + Right);
      elsif Right > Left then
         return -(Right - Left);
      end if;
      return Normalize ((Nominator  => Left.Nominator * Right.Denominator - Right.Nominator * Left.Denominator,
                         Denominator => Left.Denominator * Right.Denominator,
                         Negative    => False));
   end "-";

   function "-" (Left : Fraction; Right : Integer) return Fraction is
   begin
      return Left - Fraction'(Nominator   => abs (Right),
                              Denominator => 1,
                              Negative    => (Right < 0));
   end "-";

   function "-" (Left : Integer; Right : Fraction) return Fraction is
   begin
      return Right - Left;
   end "-";

   function "/" (Left, Right : Integer) return Fraction is
   begin
      return Create (Left, Right);
   end "/";

   function "/" (Left, Right : Fraction) return Fraction is
   begin
      return Left * Inverse (Right);
   end "/";

   function "/" (Left : Fraction; Right : Integer) return Fraction is
   begin
      if Right = 0 then
         raise Division_By_Zero;
      end if;
      return Left * Fraction'(Nominator   => 1,
                              Denominator => abs (Right),
                              Negative    => (Right < 0));
   end "/";

   function "/" (Left : Integer; Right : Fraction) return Fraction is
   begin
      return Inverse (Right) * Left;
   end "/";

   function "<" (Left, Right : Fraction) return Boolean is
   begin
      return Sign (Left) * Left.Nominator * Right.Denominator < Sign (Right) * Right.Nominator * Left.Denominator;
   end "<";

   function "<" (Left : Fraction; Right : Integer) return Boolean is
   begin
      return Sign (Left) * Left.Nominator < Right * Left.Denominator;
   end "<";

   function "<" (Left : Fraction; Right : Float) return Boolean is
   begin
      return To_Float (Left) < Right;
   end "<";

   function "<=" (Left, Right : Fraction) return Boolean is
   begin
      return Left = Right or else Left < Right;
   end "<=";

   function "<=" (Left : Fraction; Right : Integer) return Boolean is
   begin
      return Left = Right or else Left < Right;
   end "<=";

   function "<=" (Left : Fraction; Right : Float) return Boolean is
   begin
      return Left = Right or else Left < Right;
   end "<=";

   overriding function "="
     (Left, Right : Fraction)
      return Boolean
   is
   begin
      return Left.Negative = Right.Negative and then
      Left.Nominator * Right.Denominator = Right.Nominator * Left.Denominator;
   end "=";

   function "=" (Left : Fraction; Right : Integer) return Boolean is
   begin
      return Sign (Left) * Left.Nominator = Right * Left.Denominator;
   end "=";

   function "=" (Left : Fraction; Right : Float) return Boolean is
   begin
      return To_Float (Left) = Right;
   end "=";

   function ">" (Left, Right : Fraction) return Boolean is
   begin
      return Sign (Left) * Left.Nominator * Right.Denominator > Sign (Right) * Right.Nominator * Left.Denominator;
   end ">";

   function ">" (Left : Fraction; Right : Integer) return Boolean is
   begin
      return Sign (Left) * Left.Nominator > Right * Left.Denominator;
   end ">";

   function ">" (Left : Fraction; Right : Float) return Boolean is
   begin
      return To_Float (Left) > Right;
   end ">";

   function ">=" (Left, Right : Fraction) return Boolean is
   begin
      return Left = Right or else Left > Right;
   end ">=";

   function ">=" (Left : Fraction; Right : Integer) return Boolean is
   begin
      return Left = Right or else Left > Right;
   end ">=";

   function ">=" (Left : Fraction; Right : Float) return Boolean is
   begin
      return Left = Right or else Left > Right;
   end ">=";

   function Create
     (Nominator   : Integer;
      Denominator : Integer := 1)
      return Fraction
   is
   begin
      if Denominator = 0 then
         raise Division_By_Zero;
      end if;
      return Normalize ((Nominator   => abs (Nominator),
                         Denominator => Denominator,
                         Negative    => Nominator < 0 xor Denominator < 0));
   end Create;

   function Denominator (X : Fraction) return Positive is
   begin
      return X.Denominator;
   end Denominator;

   function Image (X : Fraction) return String is
   begin
      return Integer'Image (Sign (X) * X.Nominator) & " /" & Natural'Image (X.Denominator);
   end Image;

   function Inverse (X : Fraction) return Fraction is
   begin
      if X.Nominator = 0 then
         raise Division_By_Zero;
      end if;
      return Fraction'(Nominator   => X.Denominator,
                       Denominator => X.Nominator,
                       Negative    => X.Negative);
   end Inverse;

   function Nominator (X : Fraction) return Integer is
   begin
      return Sign (X) * Integer (X.Nominator);
   end Nominator;

   procedure Normalize (X : in out Fraction) is
      function Greatest_Common_Divisor (A, B : Positive) return Positive;
      function Greatest_Common_Divisor (A, B : Positive) return Positive is
         Dividend : Positive := A;
         Divisor  : Positive := B;
         Temporary : Positive;
      begin
         if A < B then
            Dividend := B;
            Divisor := A;
         end if;
         while Dividend mod Divisor /= 0 loop
            Temporary := Dividend;
            Dividend := Divisor;
            Divisor := Temporary mod Divisor;
         end loop;
         return Divisor;
      end Greatest_Common_Divisor;
      Divisor : constant Positive := Greatest_Common_Divisor (X.Nominator, X.Denominator);
   begin
      X.Nominator := X.Nominator / Divisor;
      X.Denominator := X.Denominator / Divisor;
   end Normalize;

   function Normalize (X : Fraction) return Fraction is
      Result : Fraction := X;
   begin
      Normalize (Result);
      return Result;
   end Normalize;

   function Sign (X : Fraction) return Integer is
   begin
      if X.Negative then
         return -1;
      else
         return 1;
      end if;
   end Sign;

   function Sign (X : Fraction) return Float is
   begin
      if X.Negative then
         return -1.0;
      else
         return 1.0;
      end if;
   end Sign;

   function To_Float (X : Fraction) return Float is
   begin
      return Sign (X) * Float (X.Nominator) / Float (X.Denominator);
   end To_Float;

end Fractions;
