--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
with Triangular;
procedure Euler018 is
   Example : Triangular.Pyramid;
--     Source_1  : constant array (Positive range <>) of Natural :=
--       (3,
--        7, 4,
--        2, 4, 6,
--        8, 5, 9, 3);
--     Source_2 : constant array (Positive range <>) of Natural :=
--       (01,
--        02, 01,
--        01, 01, 09,
--        01, 01, 01, 09);
   Source : constant array (Positive range <>) of Natural :=
     (75,
      95, 64,
      17, 47, 82,
      18, 35, 87, 10,
      20, 04, 82, 47, 65,
      19, 01, 23, 75, 03, 34,
      88, 02, 77, 73, 07, 63, 67,
      99, 65, 04, 28, 06, 16, 70, 92,
      41, 41, 26, 56, 83, 40, 80, 70, 33,
      41, 48, 72, 33, 47, 32, 37, 16, 94, 29,
      53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14,
      70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57,
      91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48,
      63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31,
      04, 62, 98, 27, 23, 09, 70, 98, 73, 93, 38, 53, 60, 04, 23);
   Source_Index : Positive := Source'First;

   function Maximum (Left, Right : Natural) return Natural is
   begin
      if Left > Right then
         return Left;
      else
         return Right;
      end if;
   end Maximum;
begin
   -- initialize pyramid
   Example.Create_Pyramid (15);

   -- fill pyramid
   for Row in 1 .. Example.Row_Count loop
      for Column in 1 .. Example.Column_Count (Row) loop
         Example.Set (Row, Column, Source (Source_Index));
         Source_Index := Source_Index + 1;
      end loop;
   end loop;

   -- output pyramid
   for Row in 1 .. Example.Row_Count loop
      for Column in 1 .. Example.Column_Count (Row) loop
         Ada.Text_IO.Put (Integer'Image (Example.Get (Row, Column)));
      end loop;
      Ada.Text_IO.New_Line;
   end loop;

   for Row in reverse 2 .. Example.Row_Count loop
      for Column in 2 .. Example.Column_Count (Row) loop
         Example.Set (Row    => Row - 1,
                      Column => Column - 1,
                      Value  => Example.Get (Row    => Row - 1,
                                             Column => Column - 1) +
                        Maximum
                          (Example.Get (Row    => Row,
                                        Column => Column),
                           Example.Get (Row    => Row,
                                        Column => Column - 1)));
      end loop;
   end loop;
   Ada.Text_IO.Put_Line ("Maximum:" & Integer'Image (Example.Get (1, 1)));
end Euler018;
