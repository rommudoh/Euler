--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Containers.Indefinite_Vectors;
with Ada.Strings.Fixed;
with Ada.Text_IO;
procedure Euler022 is
   package String_Vectors is new Ada.Containers.Indefinite_Vectors
     (Element_Type => String, Index_Type => Positive);
   package String_Sort is new String_Vectors.Generic_Sorting;
   function Alphabetical_Value (Item : String) return Natural is
      Result : Natural := 0;
   begin
      for I in Item'Range loop
         Result := Result + Character'Pos (Item (I)) - Character'Pos ('A') + 1;
      end loop;
      return Result;
   end Alphabetical_Value;
   File : Ada.Text_IO.File_Type;
   Names : String_Vectors.Vector;
   Sum : Natural := 0;
begin
   -- open file
   Ada.Text_IO.Open (File => File,
                     Mode => Ada.Text_IO.In_File,
                     Name => "names.txt");
   -- read content (only one line)
   declare
      Whole_Line : constant String := Ada.Text_IO.Get_Line (File => File);
      From, To   : Natural := Whole_Line'First;
      Done       : Boolean := False;
   begin
      -- close file
      Ada.Text_IO.Close (File => File);
      -- split by "," and add names to list
      while not Done loop
         To := Ada.Strings.Fixed.Index (Source  => Whole_Line,
                                              Pattern => ",",
                                        From    => From);
         -- last name has no "," afterwards, fix it
         if To < 1 then
            To := Whole_Line'Last + 1;
            Done := True;
         end if;
         Names.Append (New_Item => Whole_Line (From + 1 .. To - 2));
         From := To + 1;
      end loop;
   end;
   -- sort alphabetically
   String_Sort.Sort (Container => Names);
   -- create sum
   for I in Names.First_Index .. Names.Last_Index loop
      Sum := Sum + Alphabetical_Value (Names.Element (I)) * I;
   end loop;
   Ada.Text_IO.Put_Line ("Sum:" & Integer'Image (Sum));
end Euler022;
