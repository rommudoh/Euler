--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

--  with Ada.Containers.Ordered_Maps;
--  with Ada.Containers.Vectors;
with Ada.Text_IO;
procedure Euler322 is
   type Number is mod 2**64;
   function Binomial (N, K : Number) return Number is
      Result : Number := 1;
   begin
      if K > N then
         raise Constraint_Error;
      end if;
      if K = 0 then
         return 1;
      elsif N = 0 then
         return 0;
      end if;
      if K > N - K then
         -- binomial symmetry
         return Binomial (N, N - K);
      end if;
      for I in 1 .. K loop
         Result := Result * (N - I) / (I + 1);
      end loop;
      return Result;
   end Binomial;

   function T (M, N : Positive) return Natural is
      Result : Natural := 0;
   begin
      for I in N .. M - 1 loop
         if Binomial (N => Number (I),
                      K => Number (N)) mod 10 = 0
         then
            Result := Result + 1;
         end if;
      end loop;
      return Result;
   end T;
begin
   Ada.Text_IO.Put_Line ("Euler 322:");
   Ada.Text_IO.Put_Line (Natural'Image (T (M => 10 ** 9,
                                           N => 10 ** 7 - 10)));
end Euler322;
