--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

package body Permutations is
   procedure Insertion_Sort (Item : in out Permutation) is
      First : constant Natural := Item'First;
      Last  : constant Natural := Item'Last;
      Value : Element_Type;
      J     : Integer;
   begin
      for I in (First + 1) .. Last loop
         Value := Item (I);
         J := I - 1;
         while J in Item'Range and then Item (J) > Value loop
            Item (J + 1) := Item (J);
            J := J - 1;
         end loop;
         Item (J + 1) := Value;
      end loop;
   end Insertion_Sort;
   procedure Reset (Object : in out Permutation) is
   begin
      Insertion_Sort (Object);
   end Reset;
   procedure Swap (Left, Right : in out Element_Type) is
      Temporary : constant Element_Type := Left;
   begin
      Left := Right;
      Right := Temporary;
   end Swap;
   procedure Revert (Object : in out Permutation) is
      Left : Positive := Object'First;
      Right : Positive := Object'Last;
   begin
      loop
         exit when Left >= Right;
         Swap (Object (Left), Object (Right));
         Left := Left + 1;
         Right := Right - 1;
      end loop;
   end Revert;
   procedure Next (Object : in out Permutation) is
      K, L : Natural := Object'Last;
   begin
      -- K = largest index with Elements K < K+1
      begin
         while K > 0 loop
            K := K - 1;
            exit when Object (K) < Object (K + 1);
         end loop;
      exception
         when Constraint_Error =>
            K := 0;
      end;
      if K = 0 then
         raise Last_Permutation;
      end if;
      -- L = largest index with Elements K < L
      while L > K loop
         exit when Object (K) < Object (L);
         L := L - 1;
      end loop;
      -- swap elements K and L
      Swap (Object (K), Object (L));
      -- reverse elements K+1 .. Last
      Revert (Object (K + 1 .. Object'Last));
   end Next;
end Permutations;
