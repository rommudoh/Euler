--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler030 is
   function Is_Fifth_Grade_Power (N : Natural) return Boolean is
      Value : Natural := N;
      Factor : Natural;
      Sum : Natural := 0;
   begin
      while Value /= 0 loop
         Factor := Value mod 10;
         Value := Value / 10;
         Sum := Sum + Factor ** 5;
         exit when Sum > N;
      end loop;
      return Sum = N;
   end Is_Fifth_Grade_Power;
   Sum : Natural := 0;
begin
   for I in 2 .. 1000000 loop
      if Is_Fifth_Grade_Power (I) then
         Sum := Sum + I;
      end if;
   end loop;
   Ada.Text_IO.Put_Line ("Euler 30:" & Integer'Image (Sum));
end Euler030;
