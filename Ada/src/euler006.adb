--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler006 is
   function Sum_Of_Squares (From, To : Natural) return Natural is
      Sum : Natural := 0;
   begin
      for I in From .. To loop
         Sum := Sum + I ** 2;
      end loop;
      return Sum;
   end Sum_Of_Squares;
   function Squares_Of_Sum (From, To : Natural) return Natural is
      Sum : Natural := 0;
   begin
      for I in From .. To loop
         Sum := Sum + I;
      end loop;
      return Sum ** 2;
   end Squares_Of_Sum;
begin
   Ada.Text_IO.Put_Line ("Difference:" & Integer'Image (Squares_Of_Sum (1, 100) - Sum_Of_Squares (1, 100)));
end Euler006;
