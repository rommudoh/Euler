--
-- Copyright (c) 2016 Julian Leyh <julian@vgai.de>
--
-- Permission to use, copy, modify, and distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
-- WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
-- MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
-- ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
-- ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
-- OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--

with Ada.Text_IO;
procedure Euler214 is
   function φ (N : Positive) return Positive is
      Value  : Positive := N;
      Result : Positive := N;
      I      : Positive := 2;
   begin
      while I * I <= Value loop
         if Value mod I = 0 then
            Result := Result - Result / I;
         end if;
         while Value mod I = 0 loop
            Value := Value / I;
         end loop;
         I := I + 1;
      end loop;
      if Value > 1 then
         Result := Result - Result / Value;
      end if;
      return Result;
   end φ;

   Maximum_Prime : constant := 41_000_000;
   type Bool_Array is array (Positive range <>) of Boolean;
   type Bool_Array_Access is access Bool_Array;
   Primes        : constant Bool_Array_Access := new Bool_Array (2 .. Maximum_Prime);
   Last_Prime    : Positive := 1;

   function Get_Next_Prime return Positive is
      Next : Positive := Last_Prime + 1;
   begin
      while not Primes (Next) loop
         Next := Next + 1;
      end loop;
      declare
         Remove : Positive := Next;
      begin
         loop
            Remove := Remove + Next;
            exit when Remove > Primes'Last;
            Primes (Remove) := False;
         end loop;
      end;
      Last_Prime := Next;
      return Last_Prime;
   end Get_Next_Prime;

   Phi    : Positive;
   type Big is mod 2**64;
   Sum    : Big := 0;
   Prime : Positive;
begin
   for I in Primes'Range loop
      Primes (I) := True;
   end loop;
   Prime := Get_Next_Prime;
   Ada.Text_IO.Put_Line ("Euler214:");
   while Prime < 4E7 loop
      declare
         Sequence_Length : Positive := 1;
      begin
         Phi := Prime;
         while Phi /= 1 and then Sequence_Length <= 25 loop
            Phi := φ (Phi);
            Sequence_Length := Sequence_Length + 1;
         end loop;
         if Sequence_Length = 25 then
            -- Ada.Text_IO.Put_Line ("Prime" & Integer'Image (Prime) & " has sequence length of" & Integer'Image (Sequence_Length));
            Sum := Sum + Big (Prime);
         end if;
      end;
      Prime := Get_Next_Prime;
   end loop;
   Ada.Text_IO.Put_Line ("Sum:" & Big'Image (Sum));
end Euler214;
